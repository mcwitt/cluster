import sys
from atools import itergroups
from itertools import cycle
from scipy.interpolate import UnivariateSpline
from pylab import *

Tc_d = { 5: 8.7785, 6: 10.8346 }

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

rc('lines', markersize=8)

def plot_data(ax, x, y, yerr, splinepoints=10000, marker='o', color='b', label=None):

    ls = '-' if splinepoints == 0 else ''

    ax.errorbar(x, y, yerr, label=label, ls=ls, color=color, marker=marker,
            markeredgecolor=color, markerfacecolor='none')

    if splinepoints > 0:
        try:
            f = UnivariateSpline(x, y, k=3, s=0)
            xs = linspace(x[0], x[-1], splinepoints)
            ys = f(xs)
            ax.plot(xs, ys, color=color)
        except Exception:
            pass

files = sys.argv[1:] if len(sys.argv) > 1 else ['average.npy']

for f in files:

    data = np.load(f)
    data = data[data['k'] == 0]   # only plot k=0

    for (d,), data in itergroups(data, 'd'):

        fig_g = figure()
        ax_g = fig_g.add_subplot(111)
        fig_chi = figure()
        ax_chi = fig_chi.add_subplot(111)

        Tc = Tc_d[d]

        for ((L,), s), marker, color in zip(
            itergroups(data, 'L'),
            cycle(markers), cycle(colors)):

            N = L**d
            s = s[s['oct'] == max(s['oct'])]    # only plot data from last octave

            t = (s['T']-Tc)/Tc

            plot_data(ax_g, s['T'], s['g'], s['g_err'],
                    color=color, marker=marker, label='$L=%d$' % L)

            plot_data(ax_chi, t, s['chi']*N, s['chi_err']*N,
                    color=color, marker=marker, label='$L=%d$' % L)

        for ax in [ax_g, ax_chi]:
            ax.set_xlabel('$t$')
            ax.legend(loc='best', title=r'$d=%d$' % d)

        ax_g.set_ylabel('$g$')
        ax_chi.set_ylabel('$\chi$')

show()
