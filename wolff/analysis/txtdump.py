import sys
from prettytable import PrettyTable
from pylab import *

# (field name, description) pairs
fields = [
        ('bc',      "boundary condition; 'p': periodic, 'f': free"),
        ('d',       "number of dimensions"),
        ('L',       "linear size"),
        ('T',       "temperature"),
        ('oct',     '"octave", i.e. log_2(number of sweeps)'),
        ('nrun',    'number of independent runs for averages'),
        ('c',       'cluster size, <c>'), ('c_err', ''),
        ('mabs',    'absolute value of magnetization, <|m|>'), ('mabs_err', ''),
        ('k',       'wavevector used for measurements in columns to the right'),
        ('k2',      'squared norm of wavevector'),
        ('m2',      '2nd moment of Fourier-transformed magnetization'), ('m2_err', ''),
        ('chi',     '<m^2> - <|m|>^2, with |m| measured at k=0'), ('chi_err', ''),
        ('g',       'Binder ratio'), ('g_err', ''),
        ('dTg',     'dg/dT'), ('dTg_err', ''),
        ('d2Tg',    'd^2g/dT^2'), ('d2Tg_err', ''),
        ]

field_names = zip(*fields)[0]

# print header
for i, (field, description) in enumerate(fields):
    print '# %2d    %-9s %s' % (i, field, description)

for f in sys.argv[1:]:
    dat = np.load(f)
    tab = PrettyTable(field_names=field_names)

    for rec in dat:
        row = [rec[f] for f in field_names]
        tab.add_row(row)

    tab.border = False
    print tab

