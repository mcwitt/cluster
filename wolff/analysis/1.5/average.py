import sys
from atools import group_stats
from atools.resamp import jackknife_wrapper as jk
from binder import binder, dTg, d2Tg
from pylab import *

cols_to_average = ['M2', 'MABS', 'C', 'K10_M2']

def mean_err(y):
    'Return the mean and standard error of a data set'
    return np.average(y), np.std(y) / sqrt(len(y)-1)

def binder2(m2, m4):
    'Binder cumulant for a 2-component order parameter'
    return 2. - m4/m2**2

def chi(m2, mabs): return m2 - mabs**2

def dTm2(u, m2, m2u, T): return (m2u - m2*u)/T**2

if __name__=='__main__':
    infile  = sys.argv[1] if len(sys.argv) > 1 else 'raw.dat'
    outfile = sys.argv[2] if len(sys.argv) > 2 else 'average.npy'
    data = np.genfromtxt(infile, names=True, dtype=None)

    stats = [(col, mean_err, (col.lower(), col.lower() + '_err')) for col in cols_to_average]

    stats.extend([
                (cols_to_average[0],    len,        'nsamp'),
                (('M2', 'MABS'),        jk(chi),    ('chi', 'chi_err')),
                (('M2', 'M4'),          jk(binder), ('g', 'g_err')),
                (('U',
                  'M2', 'M4',
                  'M2U', 'M4U',
                  'T'),
                                        jk(dTg),    ('dTg', 'dTg_err')),
                (('U', 'U2',
                  'M2', 'M4',
                  'M2U', 'M4U',
                  'M2U2', 'M4U2',
                  'T'),
                                        jk(d2Tg),   ('d2Tg', 'd2Tg_err')),
            ])

    data = group_stats(data, ('bc', 'd', 'L', 'T', 'oct'), stats=stats)
    np.save(outfile, data)

