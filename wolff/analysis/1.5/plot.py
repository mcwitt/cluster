import sys
from atools import itergroups
from itertools import cycle
from pylab import *
from scipy.interpolate import UnivariateSpline

Tc = 10.8346
markers = cycle(['o', 's', '^', 'v', 'd', 'h'])
colors = cycle(['b', 'g', 'r', 'm', 'k'])

files = sys.argv[1:] if len(sys.argv) > 1 else ['average.npy']

for f in files:
    data = np.load(f)

    for (params, data), marker, color in zip(
        itergroups(data, 'd', 'L'),
        markers, colors):

        d, L = params
        N = L**d
        p = dict(color=color, marker=marker, markeredgecolor=color,
                markerfacecolor='none', label=r'$%d$' % L)

        # only plot data from last octave
        data = data[data['oct'] == max(data['oct'])]


        '''
        figure(1);  # plot g and spline
        errorbar(data['T'], data['g'], data['g_err'], ls='', **p)
        f = UnivariateSpline(data['T'], data['g'], k=3, s=0)
        x = linspace(data['T'][0], data['T'][-1], 100)
        plot(x, f(x), color=color)

        figure(2)   # plot chi and spline
        errorbar(data['T'], data['chi']*N**0.5, data['chi_err']*N**0.5, ls='', **p)
        f = UnivariateSpline(data['T'], data['chi']*N**0.5, k=2, s=0)
        plot(x, f(x), color=color)
        '''

        figure(3)   # plot N m^2 and spline
        x = (data['T'] - Tc)*L**2
        errorbar(x, data['m2']*N/L**2, data['m2_err']*N/L**2, ls='', **p)
        f = UnivariateSpline(x, data['m2']*N/L**2, k=2, s=0)
        xs = linspace(x[0], x[-1], 100)
        plot(xs, f(xs), color=color)

        '''
        figure(3); errorbar(data['T'], data['c']*N**0.5, data['c_err']*N**0.5, **p)
        figure(4); errorbar(data['T'], data['dTg']/N**0.5, data['dTg_err']/N**0.5, **p)
        figure(5); errorbar(data['T'], data['d2Tg']/N, data['d2Tg_err']/N, **p)

        figure(6); errorbar(data['T'], data['k0_m2']/N**0.5, data['k0_m2_err']/N**0.5, **p)
        figure(7); errorbar(data['T'], data['k1_m2']/N**0.5, data['k1_m2_err']/N**0.5, **p)
        figure(8); errorbar(data['T'], data['k2_m2']/N**0.5, data['k2_m2_err']/N**0.5, **p)

        figure(9); errorbar(data['T'], data['k0_dTm2']/N**0.5, data['k0_dTm2_err']/N**0.5, **p)
        figure(10); errorbar(data['T'], data['k1_dTm2']/N**0.5, data['k1_dTm2_err']/N**0.5, **p)
        figure(11); errorbar(data['T'], data['k2_dTm2']/N**0.5, data['k2_dTm2_err']/N**0.5, **p)
        '''

'''
figure(1)
xlabel('$T$')
ylabel('$g$')
legend(loc='best', title=r'$L$')

figure(2)
xlabel('$T$')
ylabel('$\chi / L^{d/2}$')
legend(loc='best', title=r'$L$')
'''

figure(3)
xlabel('$L^2 (T-T_c)$')
ylabel(r'$\chi / L^2$')
legend(loc='best', title=r'$L$')

'''
figure(4)
xlabel('$T$')
ylabel('$L^{d/2} C$')
legend(loc='best', title=r'$L$')

figure(5)
xlabel('$T$')
ylabel(r'$g^{\prime} / L^{d/2}$')
legend(loc='best', title=r'$L$')

figure(6)
xlabel('$T$')
ylabel(r'$g^{\prime \prime} / L^d$')
legend(loc='best', title=r'$L$')

figure(7)
xlabel('$T$')
ylabel(r'$\chi_{\vec{k}}$')
legend(loc='best', title=r'$L$')

figure(8)
xlabel('$T$')
ylabel(r'$\chi_{\vec{k}}$')
legend(loc='best', title=r'$L$')

figure(9)
xlabel('$T$')
ylabel(r'$\chi_{\vec{k}}$')

figure(10)
xlabel('$T$')
ylabel(r'$\chi_{\vec{k}}$')
legend(loc='best', title=r'$L$')
legend(loc='best', title=r'$L$')
'''
show()
