import sys
from atools import itergroups
from itertools import cycle
from pylab import *

Tc = 10.8346    # for d=6

markers = ['o', 's', '^', 'v', 'd', 'h']
colors =  ['b', 'g', 'r', 'm', 'k']

files = sys.argv[1:] if len(sys.argv) > 1 else ['average.npy']
markers = cycle(markers)
colors  = cycle(colors)

d = -1

for f in files:
    data = np.load(f)

    for (params, data), marker, color in zip(
            itergroups(data, 'd', 'L'),
            markers, colors):

        d, L = params
        N = L**d

        # only plot last octave
        data = data[data['oct'] == max(data['oct'])]

        t = data['T'] - Tc

        d_2 = d/2
        errorbar(t * L**d_2, data['g'], data['g_err'],
                label='%d' % L, color=color, ls='',
                marker=marker, markeredgecolor=color, markerfacecolor='none')

xlabel('$L^{d/2}(T-T_c)$')
ylabel('$g$')
legend(loc='best', title=r'$d=%d,\,L$' % d)

xlim(-60, 60)
ylim(0, 1)

#show()
savefig('scaling-binder.eps')

