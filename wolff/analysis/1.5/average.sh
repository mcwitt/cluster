#! /bin/bash

out="average.npy"

path=${0%average.sh}

while getopts ":o:" opt; do
    case $opt in
        o)  out=$OPTARG;;
    esac
done

shift $((OPTIND-1))

python $path/average.py <(cat $@) $out
