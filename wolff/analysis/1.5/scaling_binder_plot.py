from __future__ import division
import sys
import numpy as np
import matplotlib.pyplot as plt
from atools import itergroups
from itertools import cycle

A = -70.
A_err = 5.
Tc = 8.778

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

fname = sys.argv[1] if len(sys.argv) == 2 else 'allruns.npy'
data = np.load(fname)
fig = plt.figure()
ax = fig.add_subplot(111)

groupby = ('d', 'L', 't_equil', 't_samp', 'binsize')
groups = list(itergroups(data, groupby))
groups = groups[-4:]    # only include largest 4 sizes

for (params, data), marker, color in zip(groups, cycle(markers), cycle(colors)):
    d, L, t_equil, t_samp, binsize = params
    x = L**(0.5*d) * (data['T'] - Tc - A/L**2)
    plt.errorbar(x, data['g'], data['gerr'],
            ls='', color=color, marker=marker, markeredgecolor=color,
            markerfacecolor='none', label=r'$%d$' % L)

plt.xlabel(r'$L^{d/2} \left( T - T_c - A/L^2\right)$')
plt.ylabel(r'$g$')
plt.text(0.7, 0.8, r'$A = %g \pm %g$' % (A, A_err), transform=ax.transAxes)
plt.legend(loc='lower left', title=r'$L$')
plt.savefig('scaling_binder.eps')
plt.show()
