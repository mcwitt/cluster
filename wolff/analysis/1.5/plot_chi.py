import sys
from atools import itergroups
from itertools import cycle
from pylab import *
from scipy.interpolate import UnivariateSpline

markers = cycle(['o', 's', '^', 'v', 'd', 'h'])
colors = cycle(['b', 'g', 'r', 'm', 'k'])

files = sys.argv[1:] if len(sys.argv) > 1 else ['average.npy']

for f in files:
    data = np.load(f)

    for (params, data), marker, color in zip(
        itergroups(data, 'd', 'L'),
        markers, colors):

        d, L = params
        N = L**d
        p = dict(color=color, marker=marker, markeredgecolor=color,
                markerfacecolor='none', label=r'$%d$' % L)

        # only plot data from last octave
        data = data[data['oct'] == max(data['oct'])]

        figure(1)   # plot chi and spline
        errorbar(data['T'], data['chi']*N**0.5, data['chi_err']*N**0.5, **p)
        #errorbar(data['T'], data['chi']*N**0.5, data['chi_err']*N**0.5, ls='', **p)
        #f = UnivariateSpline(data['T'], data['chi']*N**0.5, k=2, s=0)
        #x = linspace(data['T'][0], data['T'][-1], 100)
        #plot(x, f(x), color=color)

        figure(2)
        errorbar(data['T'], data['k0_m2']*N/L**2, data['k0_m2_err']*N/L**2, **p)

        figure(3)
        errorbar(data['T'], data['k1_m2']*N/L**2, data['k1_m2_err']*N/L**2, **p)

        figure(4)
        errorbar(data['T'], data['k2_m2']*N/L**2, data['k2_m2_err']*N/L**2, **p)

'''
        figure(5)
        errorbar(data['T'], data['k0_dTm2']*N/L**4, data['k0_dTm2_err']*N/L**4, **p)

        figure(6)
        errorbar(data['T'], data['k1_dTm2']*N/L**4, data['k1_dTm2_err']*N/L**4, **p)

        figure(7)
        errorbar(data['T'], data['k2_dTm2']*N/L**4, data['k2_dTm2_err']*N/L**4, **p)
'''

figure(1)
xlabel('$T$')
ylabel('$\chi / L^{d/2}$')
legend(loc='best', title=r'$L$')

figure(2)
xlabel('$T$')
ylabel(r'$\chi_{100000}/L^2$')
legend(loc='best', title=r'$L$')

figure(3)
xlabel('$T$')
ylabel(r'$\chi_{110000}/L^2$')
legend(loc='best', title=r'$L$')

figure(4)
xlabel('$T$')
ylabel(r'$\chi_{200000}/L^2$')
legend(loc='best', title=r'$L$')

'''
figure(5)
xlabel('$T$')
ylabel(r'$\chi^{\prime}_{100000}/L^4$')
legend(loc='best', title=r'$L$')

figure(6)
xlabel('$T$')
ylabel(r'$\chi^{\prime}_{110000}/L^4$')
legend(loc='best', title=r'$L$')

figure(7)
xlabel('$T$')
ylabel(r'$\chi^{\prime}_{200000}/L^4$')
legend(loc='best', title=r'$L$')
'''

show()
