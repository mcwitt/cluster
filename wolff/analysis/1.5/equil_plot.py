from atools import itergroups
from pylab import *

def plot_equil(data):

    for (bc, d, L), s in itergroups(data, 'bc', 'd', 'L'):
        fig = figure()
        ax = fig.add_subplot(111)

        for (T,), s in itergroups(s, 'T'):
            errorbar(2**s['oct'], s['m2'], s['m2_err'],
                    label='$T=%.4f$' % T)

        text(0.5, 0.9, r'$N=%d^%d$' % (L, d),
            transform=ax.transAxes,
            horizontalalignment='center')

        xlabel('$N_{\mathrm{sweep}}$')
        ylabel('$m^2$')
        xscale('log', basex=2)
        yscale('log')
        legend(title='$T$', loc='lower right')

if __name__=='__main__':
    import sys
    infile = sys.argv[1] if len(sys.argv) > 1 else 'average.npy'
    data = np.load(infile)
    plot_equil(data)
    show()
