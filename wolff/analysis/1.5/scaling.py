import sys
from pylab import *
from matplotlib.widgets import Slider, CheckButtons
from atools import itergroups
from itertools import cycle
from atools.plotutil import set_errorbar

Tc = 8.778
y = 2.5
l = 2.
a = 0.
lims_y = [1., 3.]
lims_l = [1., 3.]
lims_a = [-5.*Tc, 5.*Tc]

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

fname = sys.argv[1] if len(sys.argv) == 2 else 'data.npy'
data = np.load(fname)

fig = figure()
axbinder = fig.add_subplot(121)
axchi = fig.add_subplot(122, sharex=axbinder)
subplots_adjust(bottom=0.23)
axslider_y = axes([0.25, 0.09, 0.60, 0.03])
axslider_l = axes([0.25, 0.06, 0.60, 0.03])
axslider_a = axes([0.25, 0.03, 0.60, 0.03])
axbutton   = axes([0.03, 0.03, 0.15, 0.09])

groupby = ('d', 'L', 'binsize')
groups = list(itergroups(data, groupby))
groups = groups[2:]

lines_binder = {}
lines_chi = {}

for (params, data), marker, color in zip(groups, cycle(markers), cycle(colors)):
    d, L, binsize = params
    x = L**y * ((data['T'] - Tc)/Tc - a/L**l)

    lines_binder[params] = axbinder.errorbar(
            x,
            data['g'],
            data['gerr'],
            ls='', color=color, marker=marker, markeredgecolor=color,
            markerfacecolor='none', label=r'$%d$' % L)

    lines_chi[params] = axchi.errorbar(
            x,
            #L**d * data['m2'] / L**y,
            #L**d * data['m2err'] / L**y,
            data['chi'] / L**y,
            data['chierr'] / L**y,
            ls='', color=color, marker=marker, markeredgecolor=color,
            markerfacecolor='none', label=r'$%d$' % L)

xlabelstr = r'$L^{y_T} \left( t - A L^{-\lambda} \right)$'
axbinder.set_xlabel(xlabelstr)
axbinder.set_ylabel('$g$')
axchi.set_xlabel(xlabelstr)
axchi.set_ylabel(r'$\chi/L^{y_T}$')
axchi.set_yscale('log')
axchi.legend(loc='best', title=r'$L$')

def update(_):
    y = slider_y.val
    l = slider_l.val
    a = slider_a.val

    for params, data in groups:
        d, L, binsize = params
        x = L**y * ((data['T'] - Tc)/Tc - a/L**l)
        set_errorbar(lines_binder[params], x, data['g'], data['gerr'])
        set_errorbar(lines_chi[params],    x, data['chi']/L**y, data['chierr']/L**y)

    draw()

def autoscale(_):
    for axes in axchi, axbinder:
        axes.relim()
        axes.autoscale_view()

    draw()

slider_y = Slider(axslider_y, '$y_T$',     min(lims_y), max(lims_y), valinit=y)
slider_l = Slider(axslider_l, '$\lambda$', min(lims_l), max(lims_l), valinit=l)
slider_a = Slider(axslider_a, '$A$',       min(lims_a), max(lims_a), valinit=a)
button = Button(axbutton, 'autoscale')
slider_y.on_changed(update)
slider_l.on_changed(update)
slider_a.on_changed(update)
button.on_clicked(autoscale)
show()
