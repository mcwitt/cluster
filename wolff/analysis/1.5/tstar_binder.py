from atools import itergroups
from atools.resamp import jackknife
from binder import binder
from scipy.interpolate import UnivariateSpline
from scipy.optimize import brentq
from pylab import *

def Tstar(T1, y1, T2, y2):
    params = dict(k=3, s=0)
    f1 = UnivariateSpline(T1, y1, **params)
    f2 = UnivariateSpline(T2, y2, **params)
    Tmin = max(min(T1), min(T2))
    Tmax = min(max(T1), max(T2))
    Tstar = brentq(lambda T: f1(T) - f2(T), Tmin, Tmax)
    return Tstar, f1(Tstar)

if __name__=='__main__':
    infile  = sys.argv[1] if len(sys.argv) > 1 else 'raw.dat'
    data = genfromtxt(infile, dtype=None, names=True)
    d = {}
    output = []

    for (bc, ndim, L, T), s in itergroups(data, 'bc', 'd', 'L', 'T', sort=True):
        s = s[s['oct'] == max(s['oct'])]    # use last half of data

        m2j = jackknife(s['M2'])
        m4j = jackknife(s['M4'])

        key = (bc, ndim, L)
        val = (T, s['seed'], binder(m2j, m4j))
        d.setdefault(key, []).append(val)

    for (bc, ndim, L), recs in d.items():

        k2L = (bc, ndim, 2*L)
        if k2L not in d: continue

        T_L, seeds_L, gjs_L = zip(*recs)
        T2L, seeds2L, gjs2L = zip(*d[k2L])

        # swap array indices so that first runs over jackknife samples,
        # second over temperatures
        gjs_L = zip(*gjs_L);  seeds_L = zip(*seeds_L)
        gjs2L = zip(*gjs2L);  seeds2L = zip(*seeds2L)

        recs = []

        for gj_L, gj2L, seed_L, seed2L in zip(gjs_L, gjs2L, seeds_L, seeds2L):
            try: recs.append(Tstar(T_L, gj_L, T2L, gj2L))
            except Exception as e:
                print 'skipping spline for L=%d (%d, %d):' \
                    % (L, seed_L[0], seed2L[0]), e
                continue

        nsamp = len(recs)
        Tstar_j, gstar_j = zip(*recs)

        result = (
                    bc, ndim, L, nsamp,
                    average(Tstar_j), sqrt(nsamp-1) * std(Tstar_j),
                    average(gstar_j), sqrt(nsamp-1) * std(gstar_j)
                 )

        output.append(result)

    np.save('Tstar_binder.npy',
                array(output, dtype=[
                    ('bc',        'S1'),
                    ('d',         'i'),
                    ('L',         'i'),
                    ('nsamp',     'i'),
                    ('Tstar',     'f8'),
                    ('Tstar_err', 'f8'),
                    ('gstar',     'f8'),
                    ('gstar_err', 'f8')
                    ]))

