from atools import itergroups, group_rows, group_resample
from atools.resamp import jackknife
from binder import *
from pylab import *

gstars = [0.25, 0.5, 0.75]

def tstar(T, y, yp, ypp, ystar):
    '''Approximate T at the intersection of the curve (T, y) with the line
    y=ystar given the first and second derivatives of y with respect to T.'''

    # find y value closest to ystar
    dy = y - ystar
    i0, dy0 = min(enumerate(dy), key=lambda e: abs(e[1]))
    T0, y0, yp0, ypp0 = [_[i0] for _ in T, y, yp, ypp]

    d = (yp0/ypp0)**2 - 2*dy0/ypp0
    if d < 0: return None

    pm = array([1, -1])
    dT = -yp0/ypp0 + pm * sqrt(d)
    dT = dT[0] if abs(dT[0]) < abs(dT[1]) else dT[1]

    return T0 + dT

if __name__=='__main__':
    infile  = sys.argv[1] if len(sys.argv) > 1 else 'raw.dat'
    data = genfromtxt(infile, dtype=None, names=True)
    
    # only use data from last octave
    data = data[data['oct'] == max(data['oct'])]

    data = group_resample(data, ('d', 'L', 'T'), [
                ('m2',   jackknife, 'm2j'),
                ('m4',   jackknife, 'm4j'),
                ('m2u',  jackknife, 'm2uj'),
                ('m4u',  jackknife, 'm4uj'),
                ('m2u2', jackknife, 'm2u2j'),
                ('m4u2', jackknife, 'm4u2j')
                ])

    output = []

    for (d, L), s in itergroups(data, 'd', 'L'):

        tstar_j = \
        [
            [
                tstar(
                    ss['T'],
                    binder(ss['m2'], ss['m4']),
                    dTg(ss['u'], ss['m2'], ss['m4'], ss['m2u'], ss['m4u'], ss['T']),
                    d2Tg(ss['u'], ss['u2'], ss['m2'], ss['m4'],
                        ss['m2u'], ss['m4u'], ss['m2u2'], ss['m4u2'], ss['T']),
                    gstar)

                for gstar in gstars
            ]
            for _, ss in itergroups(s, 'seed')
        ]

        tstar_j = array(tstar_j, dtype='f8')
        tstar_j = ma.masked_array(tstar_j, np.isnan(tstar_j))
        delta_j = tstar_j[:,0] - tstar_j[:,-1]
        sqnm1 = sqrt(len(tstar_j) - 1)

        result = [d, L] + gstars
        result.extend(tstar_j.mean(axis=0))
        result.extend(sqnm1 * tstar_j.std(axis=0))
        result.append(delta_j.mean())
        result.append(sqnm1 * delta_j.std())

        output.append(tuple(result))

    np.save('sr_binder_derivs.npy',
            array(output, dtype=[
                ('d',         'i'),
                ('L',         'i'),
                ('g1',        'f8'),
                ('g2',        'f8'),
                ('g3',        'f8'),
                ('T1',        'f8'),
                ('T2',        'f8'),
                ('T3',        'f8'),
                ('T1err',     'f8'),
                ('T2err',     'f8'),
                ('T3err',     'f8'),
                ('delta',     'f8'),
                ('delta_err', 'f8')
                ]))
