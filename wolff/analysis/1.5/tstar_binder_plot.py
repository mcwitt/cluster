from atools.fit import linfit
import pylab

def fit_Tc(L, tstar, tstar_err, p):
    tc, a, tc_err, a_err, _, q = linfit(L**-p, tstar, tstar_err)
    return tc, tc_err, a, a_err, q

def plot(data, p=5, n_exclude=1):
    p = float(p)
    d = data.view(pylab.recarray)
    df = d[n_exclude:]
    tc, tc_err, a, a_err, q = fit_Tc(df.L, df.Tstar, df.Tstar_err, p)

    ax = pylab.figure().add_subplot(111)

    pylab.errorbar(d.L**-p, d.Tstar, d.Tstar_err, ls='', marker='o',
            color='b', markeredgecolor='b', markerfacecolor='none')

    xlim = pylab.array(pylab.xlim())
    xlim[0] = 0.

    pylab.plot(xlim, tc + a*xlim, label=r'$T_c + A/L^%d$' % p)

    pylab.text(0.5, 0.95, \
                r'$T_c = %.5f \pm %.2g$' % (tc, tc_err) + '\n' \
                r'$A   = %.2e \pm %.2e$' % (a, a_err) + '\n' \
                r'$Q   = %.2g$' % q,
                horizontalalignment='center',
                verticalalignment='top',
                transform=ax.transAxes)

    pylab.xlabel(r'$1/L^%d$' % p)
    pylab.ylabel(r'$T^{\ast}$')
    pylab.legend(loc='best')

if __name__=='__main__':
    import sys
    fname = sys.argv[1] if len(sys.argv) == 2 else 'tstar_binder.npy'
    data = pylab.np.load(fname)
    plot(data)
    pylab.show()

