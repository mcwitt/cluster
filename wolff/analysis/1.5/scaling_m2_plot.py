import sys
import numpy as np
import matplotlib.pyplot as plt
from atools import itergroups
from itertools import cycle

A = -85.
A_err = 5.
Tc = 8.778

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

fname = sys.argv[1] if len(sys.argv) == 2 else 'allruns.npy'
data = np.load(fname)
fig = plt.figure()
ax = fig.add_subplot(111)

groupby = ('d', 'L', 't_equil', 't_samp', 'binsize')
groups = list(itergroups(data, groupby))
groups = groups[-5:]    # only include largest 4 sizes

for (params, data), marker, color in zip(groups, cycle(markers), cycle(colors)):
    d, L, t_equil, t_samp, binsize = params
    x = L**(0.5*d) * (data['T'] - Tc - A/L**2)
    y = L**(0.5*d) * data['m2']
    yerr = L**(0.5*d) * data['m2err']
    plt.errorbar(x, y, yerr,
            ls='', color=color, marker=marker, markeredgecolor=color,
            markerfacecolor='none', label=r'$%d$' % L)

plt.xlabel(r'$L^{d/2} \left( T - T_c - A/L^2\right)$')
plt.ylabel(r'$L^{d/2} \langle m^2 \rangle$')
plt.text(0.7, 0.8, r'$A = %g \pm %g$' % (A, A_err), transform=ax.transAxes)
#plt.text(0.7, 0.8, r'$A = %g$' % A, transform=ax.transAxes)
plt.legend(loc='lower left', title=r'$L$')
plt.savefig('scaling_m2.eps')
plt.show()
