import sys
from atools import itergroups
from itertools import cycle
from scipy.interpolate import UnivariateSpline
from pylab import *

Tc = 10.8346

markers = cycle(['o', 's', '^', 'v', 'd', 'h'])
colors = cycle(['b', 'g', 'r', 'm', 'k'])
rc('lines', markersize=8)

def plot_data(x, y, yerr, splinepoints=1000, marker='o', color='b', label=None):

    ls = '-' if splinepoints == 0 else ''

    errorbar(x, y, yerr, label=label, ls=ls, color=color, marker=marker,
            markeredgecolor=color, markerfacecolor='none')

    if splinepoints > 0:
        try:
            f = UnivariateSpline(x, y, k=3, s=0)
            xs = linspace(x[0], x[-1], splinepoints)
            ys = f(xs)
            plot(xs, ys, color=color)
        except Exception:
            pass

files = sys.argv[1:] if len(sys.argv) > 1 else ['average.npy']

for f in files:
    data = np.load(f)

    for (params, data), marker, color in zip(
        itergroups(data, 'd', 'L'),
        markers, colors):

        d, L = params
        N = L**d
        p = dict(color=color, marker=marker, label=r'$%d$' % L, splinepoints=0)

        # only plot data from last octave
        data = data[data['oct'] == max(data['oct'])]

        t = data['T'] - Tc

        r = 3
        figure(1); plot_data(t*L**r, data['chi']*N/L**r, data['chi_err']*N/L**r, **p)
        figure(2); plot_data(t*L**r, data['g'], data['g_err'], **p)
        figure(3); plot_data(t*L**r, data['dTg']/L**r, data['dTg_err']/L**r, **p)

        r = 2
        figure(4); plot_data(t*L**r, data['k10_m2']*N/L**r, data['k10_m2_err']*N/L**r, **p)
'''
        figure(5); plot_data(t*L**r, data['k0_g'], data['k0_g_err'], **p)
        figure(6); plot_data(t*L**r, data['k0_dTg']/L**r, data['k0_dTg_err']/L**r, **p)

        figure(3); plot_data(data['T'], data['c']*N**0.5, data['c_err']*N**0.5, **p)
        figure(4); plot_data(data['T'], data['dTg']/N**0.5, data['dTg_err']/N**0.5, **p)
        figure(5); plot_data(data['T'], data['d2Tg']/N, data['d2Tg_err']/N, **p)
        figure(6); plot_data(data['T'], data['k0_m2']/N**0.5, data['k0_m2_err']/N**0.5, **p)
        figure(7); plot_data(data['T'], data['k1_m2']/N**0.5, data['k1_m2_err']/N**0.5, **p)
        figure(8); plot_data(data['T'], data['k2_m2']/N**0.5, data['k2_m2_err']/N**0.5, **p)

        figure(9); plot_data(data['T'], data['k0_dTm2']/N**0.5, data['k0_dTm2_err']/N**0.5, **p)
        figure(10); plot_data(data['T'], data['k1_dTm2']/N**0.5, data['k1_dTm2_err']/N**0.5, **p)
        figure(11); plot_data(data['T'], data['k2_dTm2']/N**0.5, data['k2_dTm2_err']/N**0.5, **p)
        '''

figure(1)
xlabel('$L^{d/2}(T-T_c)$')
ylabel('$\chi / L^{d/2}$')
legend(loc='best', title=r'$L$')

figure(2)
xlabel('$L^{d/2}(T-T_c)$')
ylabel('$g$')
legend(loc='best', title=r'$L$')

figure(3)
xlabel('$L^{d/2}(T-T_c)$')
ylabel('$g^{\prime} / L^{d/2}$')
legend(loc='best', title=r'$L$')

figure(4)
xlabel('$L^2 (T-T_c)$')
ylabel('$\chi_{10} / L^2$')
legend(loc='best', title=r'$L$')
'''
figure(5)
xlabel('$L^2 (T-T_c)$')
ylabel('$g_{10}$')
legend(loc='best', title=r'$L$')

figure(6)
xlabel('$L^2 (T-T_c)$')
ylabel('$g_{10}^{\prime} / L^2$')
legend(loc='best', title=r'$L$')

figure(3)
xlabel('$T$')
ylabel('$L^{d/2} C$')
legend(loc='best', title=r'$L$')

figure(4)
xlabel('$T$')
ylabel(r'$g^{\prime} / L^{d/2}$')
legend(loc='best', title=r'$L$')

figure(5)
xlabel('$T$')
ylabel(r'$g^{\prime \prime} / L^d$')
legend(loc='best', title=r'$L$')

figure(6)
xlabel('$T$')
ylabel(r'$\chi_{\vec{k}}$')
legend(loc='best', title=r'$L$')

figure(7)
xlabel('$T$')
ylabel(r'$\chi_{\vec{k}}$')
legend(loc='best', title=r'$L$')

figure(8)
xlabel('$T$')
ylabel(r'$\chi_{\vec{k}}$')

figure(9)
xlabel('$T$')
ylabel(r'$\chi_{\vec{k}}$')
legend(loc='best', title=r'$L$')
legend(loc='best', title=r'$L$')
'''
show()
