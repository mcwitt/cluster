import sys
from pylab import *

colwidth = 14

oldnames = [
        'M2', 'M4', 'MABS', 'C', 'C2', 'C3', 'U', 'U2', 'M2U', 'M2U2',
        'M4U', 'M4U2', 'MABSU', 'MABSU2', 'K0_M2', 'K0_M4', 'K0_M2U', 'K0_M4U',
        'K0_M2U2', 'K0_M4U2']

newnames = [
        'M2', 'M4', 'MABS', 'C', 'C2', 'C3', 'U', 'U2', 'M2U', 'M2U2',
        'M4U', 'M4U2', 'MABSU', 'MABSU2', 'K10_M2', 'K10_M4', 'K10_M2U',
        'K10_M4U', 'K10_M2U2', 'K10_M4U2']

invalid = ['K0_M4', 'K0_M4U', 'K0_M4U2']

d = np.genfromtxt(sys.argv[1], names=True, dtype=None)

# print header
sys.stdout.write('# %14s %4s %4s %4s %10s %10s %4s %12s ' %
        ('ver', 'bc', 'd', 'L', 'seed', 'T', 'oct', 'cps'))

for name in newnames:
    sys.stdout.write('%*s ' % (colwidth, name))

sys.stdout.write('\n')

# print records
for r in d:
    sys.stdout.write(
        '%16s %4s %4d %4d %10u %10.6f %4d %12.6g ' % \
        tuple(r[name] for name in ['ver', 'bc', 'd', 'L', 'seed', 'T', 'oct', 'cps']))

    for name in oldnames:
        s = '%*s ' % (colwidth, '-') if name in invalid \
                else '%*e ' % (colwidth, r[name])
        sys.stdout.write(s)

    sys.stdout.write('\n')

