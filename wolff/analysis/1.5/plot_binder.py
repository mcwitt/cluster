import sys
from atools import itergroups
from itertools import cycle
from pylab import *
from scipy.interpolate import UnivariateSpline

markers = cycle(['o', 's', '^', 'v', 'd', 'h'])
colors = cycle(['b', 'g', 'r', 'm', 'k'])

files = sys.argv[1:] if len(sys.argv) > 1 else ['average.npy']

for f in files:
    data = np.load(f)

    for (params, data), marker, color in zip(
        itergroups(data, 'd', 'L'),
        markers, colors):

        d, L = params
        N = L**d
        p = dict(color=color, marker=marker, markeredgecolor=color,
                markerfacecolor='none', label=r'$%d$' % L)

        # only plot data from last octave
        data = data[data['oct'] == max(data['oct'])]

        figure(1)
        errorbar(data['T'], data['g'], data['g_err'], ls='', **p)
        f = UnivariateSpline(data['T'], data['g'], k=3, s=0)
        x = linspace(data['T'][0], data['T'][-1], 100)
        plot(x, f(x), color=color)

        figure(2); errorbar(data['T'], data['dTg']/N**0.5, data['dTg_err']/N**0.5, **p)
        figure(3); errorbar(data['T'], data['d2Tg']/N, data['d2Tg_err']/N, **p)

figure(1)
xlabel('$T$')
ylabel('$g$')
legend(loc='best', title=r'$L$')
xlim(10.7, 11.)
ylim(0, 1)
savefig('binder.eps')

figure(2)
xlabel('$T$')
ylabel(r'$g^{\prime} / L^{d/2}$')
legend(loc='best', title=r'$L$')

figure(3)
xlabel('$T$')
ylabel(r'$g^{\prime \prime} / L^d$')
legend(loc='best', title=r'$L$')

