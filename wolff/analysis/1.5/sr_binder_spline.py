from atools import itergroups, group_rows, group_resample
from atools.resamp import jackknife
from scipy.interpolate import UnivariateSpline
from scipy.optimize import brentq
from binder import *
from pylab import *

gstars = [0.25, 0.5, 0.75]

def Tstar(T, y, ystars):
    '''Approximate T at the intersection of the curve (T, y) with the line
    y=ystar using a cubic spline'''

    f = UnivariateSpline(T, y, k=3, s=0)
    return [brentq(lambda T: f(T) - ys, min(T), max(T)) for ys in ystars]

if __name__=='__main__':
    infile  = sys.argv[1] if len(sys.argv) > 1 else 'raw.dat'
    data = genfromtxt(infile, dtype=None, names=True)
    d = {}
    output = []

    for (bc, ndim, L, T), s in itergroups(data, 'bc', 'd', 'L', 'T'):
        s = s[s['oct'] == max(s['oct'])]    # use last half of data

        m2j = jackknife(s['m2'])
        m4j = jackknife(s['m4'])

        key = (bc, ndim, L)
        val = (T, s['seed'], binder(m2j, m4j))
        d.setdefault(key, []).append(val)

    for (bc, ndim, L), recs in d.items():

        T, seeds, gjs = zip(*recs)

        # swap array indices so that first runs over jackknife samples,
        # second over temperatures
        gjs = zip(*gjs)
        seeds = zip(*seeds)

        recs = []

        for gj, seed in zip(gjs, seeds):
            try: recs.append(Tstar(T, gj, gstars))
            except Exception as e:
                print 'skipping spline for L=%d (%d):' % (L, seed[0]), e
                continue

        nsamp = len(recs)
        Tstars_j = array(zip(*recs))
        delta_j = Tstars_j[0] - Tstars_j[-1]

        Tstars = average(Tstars_j, axis=1)
        delta = average(delta_j)
        Tstars_err = sqrt(nsamp-1) * std(Tstars_j, axis=1)
        delta_err = sqrt(nsamp-1) * std(delta_j)

        result = [bc, ndim, L, delta, delta_err] + gstars
        result.extend(Tstars)
        result.extend(Tstars_err)

        output.append(tuple(result))

    np.save('sr_binder_spline.npy',
            array(output, dtype=[
                ('bc',        'S1'),
                ('d',         'i'),
                ('L',         'i'),
                ('delta',     'f8'),
                ('delta_err', 'f8'),
                ('g1',        'f8'),
                ('g2',        'f8'),
                ('g3',        'f8'),
                ('T1',        'f8'),
                ('T2',        'f8'),
                ('T3',        'f8'),
                ('T1err',     'f8'),
                ('T2err',     'f8'),
                ('T3err',     'f8')
                ]))

