import sys
from atools import itergroups
from itertools import cycle
from pylab import *

Tc = 10.8346    # for d=6

markers = ['o', 's', '^', 'v', 'd', 'h']
colors =  ['b', 'g', 'r', 'm', 'k']

files = sys.argv[1:] if len(sys.argv) > 1 else ['average.npy']
markers = cycle(markers)
colors  = cycle(colors)

d = -1

for f in files:
    data = np.load(f)

    for (params, data), marker, color in zip(
            itergroups(data, 'd', 'L'),
            markers, colors):

        d, L = params
        N = L**d

        # only plot last octave
        data = data[data['oct'] == max(data['oct'])]

        t = data['T'] - Tc

        a = L**(0.5*d)
        errorbar(a*t, data['m2']*N/a, data['m2_err']*N/a,
                label='%d' % L, color=color, ls='',
                marker=marker, markeredgecolor=color, markerfacecolor='none')

xlabel('$L^{d/2}(T-T_c)$')
ylabel('$\chi / L^{d/2}$')
legend(loc='best', title=r'$d=%d,\,L$' % d)

xlim(-100, 100)
ylim(0,40)

#show()
savefig('scaling-chi-nosubt.eps')

