from atools import itergroups
from atools.fit import linfit
from itertools import cycle
from matplotlib.ticker import ScalarFormatter
from scipy.optimize import leastsq
from scipy.stats import chi2
from pylab import *

#Tc = 8.778475
Tc = 10.83435

def plot_rounding(data, nfit=4):
    data.sort(order='L')
    fitdata = data[-nfit:]

    a, b, a_err, b_err, _, Q = linfit(
            np.log(fitdata['L']),
            np.log(fitdata['delta']),
            fitdata['delta_err'] / fitdata['delta'])

    A, Aerr = np.exp(a), np.exp(a) * a_err
    yt, yterr = -b, b_err

    fig = figure()
    ax = fig.add_subplot(111)
    xlim = np.array([data['L'][0], data['L'][-1]])

    plot(xlim, A * xlim**b, color='b')

    errorbar(data['L'], data['delta'], data['delta_err'],
            ls='', color='r', marker='o')

    xscale('log', basex=2)
    yscale('log')
    ax.set_xlim(*xlim)
    ax.xaxis.set_ticks(2**arange(1, 5))
    ax.xaxis.set_major_formatter(ScalarFormatter())

    text(0.95, 0.95,
                r'$\Delta T(L) = A L^{-y_T}$' '\n'
                r'$A=%.2g \pm %.2g$' '\n'
                r'$y_T=%.3g \pm %.3g$' '\n'
                r'$Q=%.2g$' %
                (A, Aerr, yt, yterr, Q),
                verticalalignment='top',
                horizontalalignment='right',
                transform=ax.transAxes)

    xlabel(r'$L$')
    ylabel(r'$\Delta T(L)$')

    return yt

def plot_shift(data, yt, nfit=4):
    data.sort(order='L')

    fig = figure()
    ax = fig.add_subplot(111)

    p0 = [200, -200., 2.1]

    def resid(p):
        b, c, lmb = p
        delta = data['T2'] - (Tc + b*data['L']**-yt + c*data['L']**-lmb)
        resid = delta / data['T2err']
        return resid[-nfit:]

    try:
        p, covm, _, mesg, _ = leastsq(resid, p0[:], full_output=True)
        b, c, lmb = p
        berr, cerr, lmberr = sqrt(diag(covm))

        r2 = resid(p)**2
        chisq = sum(r2)
        ndf = len(r2) - len(p)
        Q = 1. - chi2.cdf(chisq, ndf)

    except Exception as e:
        print 'problem with fit:', e
        if mesg: print mesg
        b, c, lmb = p0
        berr, cerr, lmberr, Q = nan, nan, nan, nan

    xlim = array([0., data['L'][0]**-lmb])
    ax.set_xlim(*xlim)

    plot(xlim, Tc + c*xlim, color='b')

    errorbar(data['L']**-lmb, data['T2'] - b*data['L']**-yt, data['T2err'],
            ls='', color='r', marker='o')

    text(0.95, 0.95,
                r'$T^{\ast} = T_c + BL^{-y_T} + CL^{-\lambda}$' '\n'
                r'$\lambda=%.2g \pm %.2g$' '\n'
                r'$B=%.2g \pm %.2g$' '\n'
                r'$C=%.2g \pm %.2g$' '\n'
                r'$Q=%.2g$' %
                (lmb, lmberr, b, berr, c, cerr, Q),
                verticalalignment='top',
                horizontalalignment='right',
                transform=ax.transAxes)

    xlabel(r'$L^{-\lambda}$')
    ylabel(r'$T^{\ast}-BL^{-y_T}$')

if __name__=='__main__':
    import sys
    fname = sys.argv[1] if len(sys.argv) == 2 else 'sr_binder.npy'
    data = np.load(fname)
    yt = plot_rounding(data)
    plot_shift(data, yt)
    show()

