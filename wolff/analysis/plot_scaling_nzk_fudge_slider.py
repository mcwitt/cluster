import sys
from atools import itergroups
from itertools import cycle
from matplotlib.widgets import Slider, CheckButtons
from scipy.interpolate import UnivariateSpline
from pylab import *

d = 6
k = 211111

# fudge factor coefficients
A = 3.5
B = -3.5
lims_A = [-10., 10.]
lims_B = [-10., 10.]

Tc_d = { 5: 8.7785, 6: 10.8346 }
Tc = Tc_d[d]

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

rc('lines', markersize=8)

files = sys.argv[1:] if len(sys.argv) > 1 else ['average.npy']

for f in files:
    data = np.load(f)
    data = data[(data['d'] == d) & (data['k'] == k)]

    fig = figure()
    ax = fig.add_subplot(111)
    subplots_adjust(bottom=0.23)
    axslider_A = axes([0.25, 0.09, 0.60, 0.03])
    axslider_B = axes([0.25, 0.06, 0.60, 0.03])
    slider_A = Slider(axslider_A, '$A$', min(lims_A), max(lims_A), valinit=A)
    slider_B = Slider(axslider_B, '$B$', min(lims_B), max(lims_B), valinit=B)

    def update(_):

        ax.clear()

        A = slider_A.val
        B = slider_B.val

        for ((L,), s), marker, color in zip(
            itergroups(data, 'L'),
            cycle(markers), cycle(colors)):
            s = s[s['oct'] == max(s['oct'])]    # only plot data from last octave

            N = L**d

            x = (s['T']-Tc) * L**2 * (1 + A/L)
            y = s['m2']*N/L**2 * (1 + B/L)
            yerr = s['m2_err']*N/L**2

            ax.errorbar(x, y, yerr, label='$L=%d$' % L,
                    ls='', color=color, marker=marker,
                    markeredgecolor=color, markerfacecolor='none')

            # fit cubic spline to x, log(y)
            try:
                s = UnivariateSpline(x, log(y), k=3, s=0)
                xs = linspace(x[0], x[-1], 1000)
                ys = s(xs)
                ax.plot(xs, exp(ys), color=color)
            except ValueError:
                pass

        ax.set_yscale('log')
        ax.set_xlabel('$L^2 (T-T_c)$')
        ax.set_ylabel('$\chi / L^2$')
        ax.legend(loc='best', title=r'$d=%d,\,\vec{k}=(%s)$' % (d, k))

    update(0)

slider_A.on_changed(update)
slider_B.on_changed(update)
show()
