from atools import itergroups
from pylab import *
import sys

infile = sys.argv[1] if len(sys.argv) > 1 else 'average.npy'
data = np.load(infile)

for (bc, d, L), s in itergroups(data, 'bc', 'd', 'L'):
    fig = figure()
    ax = fig.add_subplot(111)

    for (T, k2), s in itergroups(s, 'T', 'k2'):
        errorbar(2**s['oct'], s['m2'], s['m2_err'],
                label='$%.4f,\quad %d$' % (T, k2))

    text(0.5, 0.9, r'$N=%d^%d$' % (L, d),
        transform=ax.transAxes,
        horizontalalignment='center')

    xlabel('$N_{\mathrm{sweep}}$')
    ylabel('$m^2$')
    xscale('log', basex=2)
    yscale('log')
    legend(title='$T,\quad k^2$', loc='lower right')

show()
