def binder(m2, m4): return 0.5 * (3. - m4/m2**2)

def dbg(u, m2, m4, m2u, m4u):
    'derivative of g with respect to beta'
    return m4/m2**2/2 * (
            + m4u/m4
            - 2.*m2u/m2
            + u
            )

def d2bg(u, u2, m2, m4, m2u, m4u, m2u2, m4u2):
    'second derivative of g with respect to beta'
    return m4/m2**2/2 * (
            + 2 * m2u2/m2
            - m4u2/m4
            + 4 * m2u*u/m2
            - 2 * m4u*u/m4
            + 4 * m2u*m4u/m2/m4
            - 6 * (m2u/m2)**2
            - u2
            )

def dTg(u, m2, m4, m2u, m4u, T):
    'derivative of g with respect to T'
    return -dbg(u, m2, m4, m2u, m4u)/T**2

def d2Tg(u, u2, m2, m4, m2u, m4u, m2u2, m4u2, T):
    'second derivative of g with respect to T'
    return d2bg(u, u2, m2, m4, m2u, m4u, m2u2, m4u2)/T**4 \
            + 2*dbg(u, m2, m4, m2u, m4u)/T**3

