import sys
from atools import itergroups
from itertools import cycle
from scipy.interpolate import UnivariateSpline
from pylab import *

Tc = 10.8346

markers = cycle(['o', 's', '^', 'v', 'd', 'h'])
colors = cycle(['b', 'g', 'r', 'm', 'k'])
rc('lines', markersize=8)

def plot_data(x, y, yerr, splinepoints=200, marker='o', color='b', label=None):

    ls = '-' if splinepoints == 0 else ''

    errorbar(x, y, yerr, label=label, ls=ls, color=color, marker=marker,
            markeredgecolor=color, markerfacecolor='none')

    if splinepoints > 0:
        try:
            f = UnivariateSpline(x, y, k=1, s=0)
            xs = linspace(x[0], x[-1], splinepoints)
            ys = f(xs)
            plot(xs, ys, color=color)
        except Exception:
            pass

files = sys.argv[1:] if len(sys.argv) > 1 else ['average.npy']

for f in files:
    data = np.load(f)
    data = data[data['k2'] == 0]

    for ((d, L), s), marker, color in zip(
        itergroups(data, 'd', 'L'),
        markers, colors):

        N = L**d
        p = dict(color=color, marker=marker, label=r'$%d$' % L)

        # only plot data from last octave
        s = s[s['oct'] == max(s['oct'])]

        t = s['T'] - Tc

        r = 3
        figure(1); plot_data(t*L**r, s['chi']*N/L**r, s['chi_err']*N/L**r, **p)
        figure(2); plot_data(t*L**r, s['g'], s['g_err'], **p)
        figure(3); plot_data(t*L**r, s['dTg']/L**r, s['dTg_err']/L**r, **p)

figure(1)
xlabel('$L^{d/2}(T-T_c)$')
ylabel('$\chi / L^{d/2}$')
legend(loc='best', title=r'$L$')

figure(2)
xlabel('$L^{d/2}(T-T_c)$')
ylabel('$g$')
legend(loc='best', title=r'$L$')

figure(3)
xlabel('$L^{d/2}(T-T_c)$')
ylabel('$g^{\prime} / L^{d/2}$')
legend(loc='best', title=r'$L$')

show()
