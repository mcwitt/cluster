import sys
from atools import itergroups
from itertools import cycle
from scipy.interpolate import UnivariateSpline
from pylab import *

Tc_d = { 5: 8.7785, 6: 10.8346 }

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

rc('lines', markersize=8)

files = sys.argv[1:] if len(sys.argv) > 1 else ['average.npy']

for f in files:
    data = np.load(f)

    for (d, k), data in itergroups(data, 'd', 'k'):

        fig = figure()
        ax = fig.add_subplot(111)

        Tc = Tc_d[d]

        for ((L,), s), marker, color in zip(
            itergroups(data, 'L'),
            cycle(markers), cycle(colors)):
            s = s[s['oct'] == max(s['oct'])]    # only plot data from last octave

            N = L**d

            x = (s['T']-Tc)*L**2
            y = s['m2']*N/L**2
            yerr = s['m2_err']*N/L**2

            errorbar(x, y, yerr, label='$L=%d$' % L,
                    ls='', color=color, marker=marker,
                    markeredgecolor=color, markerfacecolor='none')

            # fit cubic spline to x, log(y)
            try:
                s = UnivariateSpline(x, log(y), k=3, s=0)
                xs = linspace(x[0], x[-1], 1000)
                ys = s(xs)
                plot(xs, exp(ys), color=color)
            except Exception:
                pass

        yscale('log')
        xlabel('$L^2 (T-T_c)$')
        ylabel('$\chi / L^2$')
        legend(loc='best', title=r'$d=%d,\,\vec{k}=(%s)$' % (d, k))

show()
