import sys
import dataformat
import numpy as np
import matplotlib.pyplot as plt
from atools import itergroups
from resamp import jackknife_average as jka
from fit import linfit

npoints = 50  
tmax = 10000    # in units of bins
meas = 'm2'    # measurement to use

fname = sys.argv[1] if len(sys.argv) == 2 else 'autocorr.data'
data = np.loadtxt(fname, dtype=dataformat.fmt)
data.sort(order='bin')

for (d, L, t_equil, binsize, T), data in itergroups(data, ('d', 'L', 't_equil', 'binsize', 'T')):
    table = []
    for (seed,), data in itergroups(data, ('seed',)): table.append(data[meas])
    table = np.array(table).T   # first index runs over bin, second over sample
    n = len(table)
    tmax = min(tmax, n/2)
    dt = max(tmax / npoints, 1)
    plot_data = []

    for t in xrange(dt, tmax, dt):
        a = np.average(table[:n-t] * table[t:], axis=0)
        b = np.average(table, axis=0)
        yj = jka(a) - jka(b)**2
        yavg = np.average(yj)
        yerr = np.sqrt(len(yj) - 1) * np.std(yj)
        plot_data.append((t * binsize, yavg, yerr))

    t, yavg, yerr = (np.array(l) for l in zip(*plot_data))
    plt.errorbar(t, yavg, yerr)

    tcut = 0
    s2n = yavg / yerr
    while tcut < len(s2n) and s2n[tcut] > 3.: tcut += 1
    t = t[:tcut]
    yavg = yavg[:tcut]
    yerr = yerr[:tcut]
    #print tcut
    a, b, aerr, berr, chi2, Q = linfit(t, np.log(yavg), yerr/yavg)
    plt.text(0.6, 0.9, r'$L=%d$' % L, transform=plt.gca().transAxes)
    plt.text(0.6, 0.8, r'$\tau=%.1f \pm %.1g$' % (-1./b, berr/b**2),
            transform=plt.gca().transAxes)
    plt.axhline(yavg[0]/2.718)
    #plt.xscale('log')
    plt.show()



        
        


