import numpy as np
import sys
from math import *

import dataformat
from resamp import jackknife
from atools import mean_err, group_stats

def binder(m2, m4): return 0.5 * (3. - m4/m2**2)
def cluster(n, n3): return n3 / n**2

def G_weighted(G, Gi):
    Gvar, Givar = np.var(G), np.var(Gi)
    z = 1./Gvar + 1./Givar
    return (G/Gvar + Gi/Givar) / z

infile  = sys.argv[1] if len(sys.argv) > 1 else 'raw.data'
outfile = sys.argv[2] if len(sys.argv) > 2 else 'data'
data = np.loadtxt(infile, dtype=dataformat.pbc)

data = group_stats(data, ('d','L','t_equil','t_samp','binsize','T'),
    stats = [
        ('m2',          len,                    'nsamp'),
        ('m2',          mean_err,               ('m2', 'm2err')),
        ('G',           mean_err,               ('G', 'Gerr')),
        ('Gi',          mean_err,               ('Gi', 'Gierr')),
        ('n',           mean_err,               ('n', 'nerr')),
        (('m2', 'm4'),  jackknife(binder),      ('g', 'gerr')),
        (('n', 'n3'),   jackknife(cluster),     ('h', 'herr')),
        (('G', 'Gi'),   jackknife(G_weighted),  ('Gw', 'Gwerr'))
    ])

np.save(outfile, data)
