import numpy as np

pbc =   [
        ('d',       'i'),
        ('L',       'i'),
        ('t_equil', 'i'),
        ('t_samp',  'i'),
        ('binsize', 'i'),
        ('seed',    'i'),
        ('cputime', 'f8'),
        ('T',       'f8'),
        ('bin',     'i'),
        ('m2',      'f8'),
        ('m4',      'f8'),
        ('n',       'f8'),
        ('n2',      'f8'),
        ('n3',      'f8'),
        ('G',       'f8'),
        ('G2',      'f8'),
        ('Gi',      'f8'),
        ('Gi2',     'f8')
        ]

fbc_old =   [
        ('d',       'i'),
        ('L',       'i'),
        ('t_equil', 'i'),
        ('t_samp',  'i'),
        ('binsize', 'i'),
        ('seed',    'i'),
        ('cputime', 'f8'),
        ('run',     'i'),
        ('T',       'f8'),
        ('bin',     'i'),
        ('m2',      'f8'),
        ('m4',      'f8'),
        ('mp2',     'f8'),
        ('mp4',     'f8'),
        ('n',       'f8'),
        ('n2',      'f8'),
        ('n3',      'f8'),
        ('mabs',    'f8')
        ]

fbc =   [
        ('d',       'i'),
        ('L',       'i'),
        ('binsize', 'i'),
        ('seed',    'i'),
        ('run',     'i'),
        ('T',       'f8'),
        ('bin',     'i'),
        ('m2',      'f8'),
        ('m4',      'f8'),
        ('mp2',     'f8'),
        ('mp4',     'f8'),
        ('n',       'f8'),
        ('n2',      'f8'),
        ('n3',      'f8'),
        ('mabs',    'f8'),
        ]
