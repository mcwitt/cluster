import sys
import dataformat
import numpy as np
import matplotlib.pyplot as plt
from analyze import jka, itergroups, linfit

meas = 'G'    # measurement to use

fname = sys.argv[1] if len(sys.argv) == 2 else 'bins.data'
data = np.loadtxt(fname, dtype=dataformat.cols)
data.sort(order='bin')

for (d, L, t_equil, binsize, T), data in itergroups(data, ('d', 'L', 't_equil', 'binsize', 'T')):
    table = []
    for (seed,), data in itergroups(data, ('seed',)): table.append(data[meas])
    table = np.array(table).T   # first index runs over bin, second over sample
    table = table[:2**int(np.log(len(table))/np.log(2))]
    nbins, nsamp = table.shape
    plot_data = []
    
    while nbins > 2:
        nbins /= 2
        table = table.reshape((-1, nbins, nsamp))   # each column is a bin
        bin_avg = np.average(table, axis=0)
        var = np.average(bin_avg**2, axis=0) - np.average(bin_avg, axis=0)**2
        sigma = np.sqrt(var / (nbins-1))
        sigma_avg = np.average(sigma)
        sigma_err = np.std(sigma) / np.sqrt(len(sigma) - 1)
        plot_data.append((nbins*binsize, sigma_avg, sigma_err))

    nbins, sigma_avg, sigma_err = zip(*plot_data)
    plt.errorbar(nbins, sigma_avg, sigma_err)
    plt.show()
