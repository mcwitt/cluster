import sys
from atools import *
from pylab import *

nskip = 1   # skip initial log2 bins

fname = sys.argv[1] if len(sys.argv) == 2 else 'equil.npy'
data = np.load(fname)

class CyclePlot:
    def __init__(self, fig, sT):
        self.ax = fig.add_subplot(111)
        self.sT = sT
        self.iT = 0
        fig.canvas.mpl_connect('key_press_event', self.on_key)
        self.update()

    def on_key(self, event):
        if event.key == 'j':   self.iT -= 1
        elif event.key == 'k': self.iT += 1
        self.iT %= len(self.sT)
        self.update()

    def update(self):
        s = self.sT[self.iT]
        ax = self.ax

        ax.clear()
        ax.errorbar(2**s['log2bin'] * binsize, s['m2'], s['m2err'])
        ax.text(0.5, 0.8, r'$N=%d^%d \quad T=%.4f$' % tuple(s[a][0] for a in 'LdT'),
            transform=ax.transAxes, horizontalalignment='center')
        ax.set_xlabel('$N_{\mathrm{clust}}$')
        ax.set_ylabel('$m^2$')
        ax.set_xscale('log')
        #ax.set_yscale('log')

        draw()

plots = []

for (d, L, binsize), s in itergroups(data, ('d', 'L', 'binsize')):

    _, sT = zip(*groups(s[nskip:], ('T',)))
    fig = figure()
    plots.append(CyclePlot(fig, sT))

show()
