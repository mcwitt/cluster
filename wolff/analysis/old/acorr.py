import dataformat
from atools import itergroups
from matplotlib.widgets import Cursor
from pylab import *

meas = ['m2', 'G', 'Gi', 'n']

class EquilPlot:
    fig = figure()
    ax = fig.add_subplot(111)
    cursor = Cursor(ax, useblit=True, color='b')

    def __init__(self, data, set_equil_time):

            fig = figure()
            ax = fig.add_subplot(111)

            for (T,), data in itergroups(data, ('T',)):
                errorbar(2**data['oct'] * binsize, data['m2'], data['m2err'],
                        label='$T=%.4f$' % T)

            text(0.5, 0.9, r'$N=%d^%d$' % (L, d),
                transform=ax.transAxes,
                horizontalalignment='center')

            xlabel('$N_{\mathrm{sweep}}$')
            ylabel('$m^2$')
            xscale('log', basex=2)
            #yscale('log')
            legend(title='$T$', loc='lower right')

        def onclick(event):
            data['tequil'] = event.x
            print event.x

        fig.canvas.mpl_connect('button_press_event', onclick)


def acorr(y, t):
    n = len(y)
    return sum(y[:n-t]*y[t:]) - \
        sum(y[:n-t])*sum(y[t:]) / (n-t)

def acorr_time_integrated(y):
    n = len(y)
    c = [acorr(y, t) for t in xrange(n)]
    return sum(c) / c[0]

def find_acorrs(data, equil_time):

    for key, data in itergroups(data, ('T',)):
        row = list(key)
        binsize = data['binsize'][0]

        table = []
        for key, data in itergroups(data, ('seed',)):
            table.append([binsize * acorr_time_integrated(data[m]) for m in meas])

        for tau_m in zip(*table):
            row.append(np.average(tau_m))
            row.append(np.std(tau_m)/sqrt(len(tau_m)-1))

        rows.append(row)

    names = list(groupby)
    for m in meas:
        names.append('tau_%s' % m)
        names.append('tau_%s_err' % m)

    data = np.rec.fromrecords(rows, names=names)
    return data

if __name__ == '__main__':
    import sys
    infile  = sys.argv[1] if len(sys.argv) > 1 else 'warmup.data'
    outfile = sys.argv[2] if len(sys.argv) > 2 else 'acorr.npy'
    data = np.loadtxt(infile, dtype=dataformat.fmt)
    data.sort(order='bin')

    plots = []

    for (d, L, binsize), data in itergroups(data, ('d', 'L', 'binsize')):

        def set_equil_time(t):
            data = find_acorrs(data, t)
            save(outfile, data)

        plots.append(EquilPlot(data, set_equil_time))

    show()

