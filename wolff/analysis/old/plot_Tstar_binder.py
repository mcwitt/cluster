import sys
import dataformat
import numpy as np
import matplotlib.pyplot as plt
import analyze as a

Tc = 8.778  # use as guess for root finding

fname = sys.argv[1] if len(sys.argv) == 2 else 'all.data'
data = np.loadtxt(fname, dtype=dataformat.cols)

databysize = []

for (L,), data in a.itergroups(data, ('L',)):
    assert len(np.unique(data['t_equil'])) == 1
    assert len(np.unique(data['binsize'])) == 1
    gj_data = []

    for (T,), data in a.itergroups(data, ('T',)):
        n = len(data)
        gj = a.binderg(a.jkavg(data['m2']), a.jkavg(data['m4']))
        gj_data.append((T, gj))

    T, gj = zip(*gj_data)
    databysize.append((L, T, gj))

plot_data = []

for i in xrange(len(databysize)-1):
    L1, T1, gj1 = databysize[i]
    L2, T2, gj2 = databysize[i+1]
    result = a.interp_intersection(T1, T2, gj1, gj2, Tc)
    Tstar, Tstar_err, gstar, gstar_err = result
    plot_data.append((L1, Tstar, Tstar_err, gstar, gstar_err))

L, Tstar, Tstar_err, gstar, gstar_err = zip(*plot_data)
L = np.array(L)
plt.errorbar(1./L, Tstar, Tstar_err)
plt.xlabel('$1/L$')
plt.ylabel(r'$T^{\ast}$')

plt.show()
    

