import sys
import scipy.stats as stats
import scipy.optimize as opt
import fit
from atools import itergroups
from itertools import cycle
from pylab import *

Tc = 8.778
nfit = 6
c0 = [-50, 500, 2]

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

fname = sys.argv[1] if len(sys.argv) == 2 else 'chi.npy'
data = np.load(fname)

for ((d,), data), marker, color in zip(
    itergroups(data, ('d')),
    cycle(markers), cycle(colors)):

    L = data['L']

    def resids(c):
        a1, a2, p = c
        resids = (data['Ts'] - (Tc + a1*L**-p + a2*L**(-2*p))) / data['Tserr']
        return resids[-nfit:]

    c, covm = opt.leastsq(resids, c0[:], full_output=True)[:2]
    a1, a2, p = c
    a1err, a2err, perr = np.sqrt(np.diag(covm))
    r2 = resids(c)**2
    chisq = sum(r2)
    ndf = len(r2) - len(c)
    Q = 1. - stats.chi2.cdf(chisq, ndf)

    errorbar(L**-p, data['Ts'], data['Tserr'],
            ls='', color=color, marker=marker,
            markeredgecolor=color, markerfacecolor='none')

    x = linspace(0, 1.1 * min(L)**-p, 100)
    plot(x, Tc + a1*x + a2*x**2)
    xlim(x[0], x[-1])
    ylim(7.9, 8.8)

ax = gca()
text(0.6, 0.9,  r'$T^{\ast}(L) = T_c + A_1 L^{-\lambda} + A_2 L^{-2 \lambda}$',
        transform=ax.transAxes)
text(0.6, 0.85, r'$A_1=%.2g \pm %.2g$' % (a1, a1err), transform=ax.transAxes)
text(0.6, 0.8,  r'$A_2=%.2g \pm %.2g$' % (a2, a2err), transform=ax.transAxes)
text(0.6, 0.75, r'$\lambda=%.2g \pm %.2g$' % (p, perr), transform=ax.transAxes)
text(0.6, 0.7,  r'$Q=%.2g$' % Q, transform=ax.transAxes)
xlabel(r'$L^{-\lambda}$')
ylabel(r'$T^{\ast}$')
show()

