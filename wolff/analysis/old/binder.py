import sys
import dataformat
from atools import itergroups
from resamp import jackknife_average
from scipy.interpolate import UnivariateSpline
from scipy.optimize import brentq
from pylab import *

g = [0.25, 0.5, 0.75]

def findx(x, y, yvals, k=3, s=0):
    f = UnivariateSpline(x, y, k=k, s=s)
    return [brentq(lambda x: f(x) - y, x[0], x[-1]) for y in yvals]

if __name__=='__main__':
    infile  = sys.argv[1] if len(sys.argv) > 1 else 'raw.dat'
    outfile = sys.argv[2] if len(sys.argv) > 2 else 'binder'
    data = genfromtxt(infile, usecols=range(15), dtype=dataformat.fbc)

    data = data[data['bin'] > 0]
    oct = (log(data['bin'])/log(2)).astype(int)
    data = rec_append_fields(data, 'oct', oct)
    data = data[data['oct'] == max(data['oct'])]

    rows = []

    for (d, L), data in itergroups(data, ('d', 'L')):

        pairs = [(T, jackknife_average(0.5 * (3. - data['m4']/data['m2']**2)))
                    for (T,), data in itergroups(data, ('T',))]

        T, gj = zip(*pairs)
        gj = zip(*gj)   # first index runs over jackknife samples
        n = len(gj)

        try: Tj = [findx(T, y, g) for y in gj]
        except Exception as e:
            print "couldn't find delta for L=%d:" % L, e
            continue

        Tj = array(Tj).T
        deltaj = Tj[0] - Tj[2]
        T = [average(Tnj) for Tnj in Tj]
        delta = average(deltaj)
        Terr = [sqrt(n-1) * std(Tnj) for Tnj in Tj]
        deltaerr = sqrt(n-1) * std(deltaj)

        result = (d, L, g[0], g[1], g[2],
                T[0], Terr[0], T[1], Terr[1], T[2], Terr[2],
                delta, deltaerr)

        rows.append(result)

    np.save(outfile, array(rows, dtype=[
                ('d',        'i'),
                ('L',        'i'),
                ('g1',       'f8'),
                ('g2',       'f8'),
                ('g3',       'f8'),
                ('T1',       'f8'),
                ('T1err',    'f8'),
                ('T2',       'f8'),
                ('T2err',    'f8'),
                ('T3',       'f8'),
                ('T3err',    'f8'),
                ('delta',    'f8'),
                ('deltaerr', 'f8'),
                ]))

