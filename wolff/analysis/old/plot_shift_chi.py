import sys
import numpy as np
import matplotlib.pyplot as plt
import fit
from atools import itergroups
from itertools import cycle

Tc = 8.778
nfit = 5

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

fname = sys.argv[1] if len(sys.argv) == 2 else 'chi.npy'
data = np.load(fname)

for ((d,), data), marker, color in zip(
    itergroups(data, ('d')),
    cycle(markers), cycle(colors)):

    data.sort(order='L')
    fitdata = data[-nfit:]

    a, b, a_err, b_err, _, Q = fit.linfit(
            np.log(fitdata['L']),
            np.log(abs(fitdata['Ts'] - Tc)),
            fitdata['Tserr']/(fitdata['Ts'] - Tc))

    A, A_err = np.exp(a), np.exp(a) * a_err
    lmd, lmd_err = -b, b_err
    if data[0]['Ts'] < Tc: A = -A

    xlim = np.array([data['L'][0], data['L'][-1]])
    dx = xlim[1] - xlim[0]

    plt.errorbar(data['L'], np.abs(data['Ts'] - Tc), data['Tserr'],
            ls='', color=color, marker=marker,
            markeredgecolor=color, markerfacecolor='none')

    plt.plot(xlim, abs(A) * xlim**b)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim(*xlim)

ax = plt.gca()
plt.text(0.7, 0.9, r'$T^{\ast}(L) - T_c = A L^{-\lambda}$', transform=ax.transAxes)
plt.text(0.7, 0.85, r'$A=%.2g \pm %.2g$' % (A, A_err), transform=ax.transAxes)
plt.text(0.7, 0.8, r'$\lambda=%.2g \pm %.2g$' % (lmd, lmd_err), transform=ax.transAxes)
plt.text(0.7, 0.75, r'$Q=%.2g$' % Q, transform=ax.transAxes)
plt.xlabel(r'$L$')
plt.ylabel(r'$\left| T^{\ast} - T_c \right|$')
plt.show()
    
