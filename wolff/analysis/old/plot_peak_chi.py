from atools import groups, itergroups
from pylab import *

def plot_peak_chi(data):

    for (d,), data in itergroups(data, ('d',)):
        fig = figure()
        ax = fig.add_subplot(111)
        errorbar(data['L'], data['chis'], data['chiserr'])
        xlabel('$L$')
        ylabel(r'$\chi^{\ast}$')

        x = (log(1. * data['L'] / min(data['L'])) / log(2))
        ints = (x % 1) < 1e-15
        x = x[ints]
        data = data[ints]
        x = x.astype(int)
        data = rec_append_fields(data, 'x', x)
        data.sort(order='x')
        points = []

        for x1, x2 in zip(data[:-1], data[1:]):
            r = x2['chis']/x1['chis']
            rerr = x2['chiserr']/x1['chis'] + x2['chis']*x1['chiserr']/x1['chis']**2
            points.append((x1['L'], r, rerr))

        l1, r, rerr = zip(*points)

        fig = figure()
        ax = fig.add_subplot(111)
        errorbar(l1, r, rerr)
        xlabel('$L$')
        ylabel(r'$\chi^{\ast}_{2L} / \chi^{\ast}_L$')
        
if __name__=='__main__':
    import sys
    infile = sys.argv[1] if len(sys.argv) > 1 else 'chi.npy'
    data = np.load(infile)
    plot_peak_chi(data)
    show()
