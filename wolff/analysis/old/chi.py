import sys
import dataformat
from atools import itergroups
from resamp import jackknife_average
from scipy.interpolate import UnivariateSpline
from scipy.optimize import brentq
from pylab import *

def find_peak(x, y, k=2, s=0):
    f = UnivariateSpline(x, y, k=k, s=s)
    xp = brentq(lambda x: f(x, 1), x[0], x[-1])
    return xp, f(xp)

if __name__=='__main__':
    infile  = sys.argv[1] if len(sys.argv) > 1 else 'raw.dat'
    outfile = sys.argv[2] if len(sys.argv) > 2 else 'chi.npy'
    data = genfromtxt(infile, dtype=dataformat.fbc)

    data = data[data['bin'] > 0]
    oct = (log(data['bin'])/log(2)).astype(int)
    data = rec_append_fields(data, 'oct', oct)
    data = data[data['oct'] == max(data['oct'])]
    #data = data[data['oct'] > 1]

    rows = []

    for params, data in itergroups(data, ('d', 'L', 'binsize')):
        d, L, binsize = params

        pairs = [(T, L**d * jackknife_average(data['m2'] - data['mabs']**2))
                    for (T,), data in itergroups(data, ('T',))]

        T, chij = zip(*pairs)
        chij = zip(*chij)
        n = len(chij)

        # chij is a 2d array with the first index running over jackknife samples
        # and the second over temperatures

        # find peak in chi
        try:
            peaks = [find_peak(T, y) for y in chij]
        except Exception as e:
            print "couldn't find peak for L=%d:" % L, e
            continue

        Tsj, chisj = zip(*peaks)
        Ts   = average(Tsj)
        chis = average(chisj)
        Tserr   = sqrt(n - 1) * std(Tsj)
        chiserr = sqrt(n - 1) * std(chisj)

        result = (d, L, binsize, Ts, Tserr, chis, chiserr)
        rows.append(result)
        print result

    np.save(outfile, array(rows, dtype=[
        ('d',       'i'),
        ('L',       'i'),
        ('binsize', 'i'),
        ('Ts',      'f8'),
        ('Tserr',   'f8'),
        ('chis',    'f8'),
        ('chiserr', 'f8'),
        ]))


