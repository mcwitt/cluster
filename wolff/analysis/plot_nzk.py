import sys
from atools import itergroups
from itertools import cycle
from scipy.interpolate import UnivariateSpline
from pylab import *

Tc_d = { 5: 8.7785, 6: 10.8346 }

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

rc('lines', markersize=8)

def plot_data(x, y, yerr, splinepoints=1000, marker='o', color='b', label=None):

    ls = '-' if splinepoints == 0 else ''

    errorbar(x, y, yerr, label=label, ls=ls, color=color, marker=marker,
            markeredgecolor=color, markerfacecolor='none')

    if splinepoints > 0:
        try:
            f = UnivariateSpline(x, y, k=3, s=0)
            xs = linspace(x[0], x[-1], splinepoints)
            ys = f(xs)
            plot(xs, ys, color=color)
        except Exception:
            pass

files = sys.argv[1:] if len(sys.argv) > 1 else ['average.npy']

for f in files:
    data = np.load(f)

    for (d, k), data in itergroups(data, 'd', 'k'):

        fig = figure()
        ax = fig.add_subplot(111)

        Tc = Tc_d[d]

        for ((L,), s), marker, color in zip(
            itergroups(data, 'L'),
            cycle(markers), cycle(colors)):
            s = s[s['oct'] == max(s['oct'])]    # only plot data from last octave

            N = L**d
            plot_data((s['T']-Tc)/Tc, s['m2']*N, s['m2_err']*N,
                    color=color, marker=marker, label='$L=%d$' % L)

        xlabel('$t$')
        ylabel('$\chi$')
        legend(loc='best', title=r'$d=%d,\,\vec{k}=(%s)$' % (d, k))

show()
