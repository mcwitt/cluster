import sys
from atools import group_stats
from atools.resamp import jackknife_wrapper as jk
from binder import *
from pylab import *

cols2average = ['M2', 'MABS', 'C']

def g(m2, m4):
    'Binder ratio'
    return 0.5 * (3. - m4/m2**2)

def dbg(u, m2, m4, m2u, m4u):
    'derivative of g with respect to beta'
    return m4/m2**2/2 * (
            + m4u/m4
            - 2.*m2u/m2
            + u
            )

def d2bg(u, u2, m2, m4, m2u, m4u, m2u2, m4u2):
    'second derivative of g with respect to beta'
    return m4/m2**2/2 * (
            + 2 * m2u2/m2
            - m4u2/m4
            + 4 * m2u*u/m2
            - 2 * m4u*u/m4
            + 4 * m2u*m4u/m2/m4
            - 6 * (m2u/m2)**2
            - u2
            )

def dTg(u, m2, m4, m2u, m4u, T):
    'derivative of g with respect to T'
    return -dbg(u, m2, m4, m2u, m4u)/T**2

def d2Tg(u, u2, m2, m4, m2u, m4u, m2u2, m4u2, T):
    'second derivative of g with respect to T'
    return d2bg(u, u2, m2, m4, m2u, m4u, m2u2, m4u2)/T**4 \
            + 2*dbg(u, m2, m4, m2u, m4u)/T**3

def chi(m2, mabs):
    'susceptibility with <|m|>^2 subtracted term'
    return m2 - mabs**2

if __name__=='__main__':
    infile  = sys.argv[1] if len(sys.argv) > 1 else 'raw.dat'
    outfile = sys.argv[2] if len(sys.argv) > 2 else 'average.npy'
    data = np.genfromtxt(infile, names=True, dtype=None)

    stats = [(cols2average[0], len, 'nrun')]

    stats.extend((col, lambda y: (np.average(y), np.std(y) / sqrt(len(y)-1)),
            (col.lower(), col.lower() + '_err')) for col in cols2average)

    stats.extend([
                (('M2', 'MABS'),    jk(chi),    ('chi', 'chi_err')),
                (('M2', 'M4'),      jk(binder), ('g', 'g_err')),
                (('U',
                  'M2', 'M4',
                  'M2U', 'M4U',
                  'T'),
                                    jk(dTg),    ('dTg', 'dTg_err')),
                (('U', 'U2',
                  'M2', 'M4',
                  'M2U', 'M4U',
                  'M2U2', 'M4U2',
                  'T'),
                                    jk(d2Tg),   ('d2Tg', 'd2Tg_err')),
            ])

    data = group_stats(data, ('bc', 'd', 'L', 'T', 'oct', 'k', 'k2'), stats=stats)
    np.save(outfile, data)

