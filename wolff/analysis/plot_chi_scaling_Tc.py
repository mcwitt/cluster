from atools import itergroups
from itertools import cycle
from pylab import *

markers = cycle(['o', 's', '^', 'v', 'd', 'h'])
colors = cycle(['b', 'g', 'r', 'm', 'k'])
rc('lines', markersize=8)

infile = sys.argv[1] if len(sys.argv) > 1 else 'average.npy'
data = np.load(infile)
data = data[data['oct'] == max(data['oct'])]
data = data[data['k2'] > 0]

for (bc, d), data in itergroups(data, 'bc', 'd'):
    fig = figure()
    ax = fig.add_subplot(111)

    for ((L,), s), marker, color in zip(
            itergroups(data, 'L'), markers, colors):
        N = L**d
        k = sqrt(s['k2'])
        chi_k = N * s['m2']
        chi_k_err = N * s['m2_err']
        errorbar(k, chi_k/L**2, chi_k_err/L**2,
                label='$%d$' % L,
                color=color, marker=marker,
                markeredgecolor=color, markerfacecolor='none')

    text(0.5, 0.9, r'$T=10.8346 \sim T_c$',
        transform=ax.transAxes,
        horizontalalignment='center')

    #xscale('log')
    #yscale('log')
    xlabel('$k$')
    ylabel(r'$\chi_{\vec{k}} / L^2$')
    legend(title='$L$', loc='best')

show()


