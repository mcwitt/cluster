#include "lattice.h"

typedef struct { double coskr[N], sinkr[N]; } wavevector_t;

void wavevector_init(wavevector_t *w, const int k[D]);

double wavevector_m2(wavevector_t *w, const int spin[N]);

