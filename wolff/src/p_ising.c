#include "ising.h"
#include <math.h>

void ising_init(ising_t *s)
{
    int a[Z][D], i, j;

    for (i = 0; i < D; i++)
    {
        for (j = 0; j < D; j++) a[i][j] = a[i+D][j] = 0;
        a[i][i] = -1;
        a[i+D][i] = 1;
    }

    lattice_set_neighbors(&s->neighbor[0][0], a, Z);
    s->energy_ini = -N*D;
}

int ising_energy(ising_t *s, const int spin[N])
{
    int i, j, energy = 0;

    for (i = 0; i < N; i++)
        for (j = 0; j < D; j++)
            energy -= spin[i] * spin[s->neighbor[i][j]];

    return energy;
}

