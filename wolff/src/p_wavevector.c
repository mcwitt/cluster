#include "p_wavevector.h"
#include <math.h>

const double a = 2. * M_PI / L;

void wavevector_init(wavevector_t *w, const int k[D])
{
    int r[D], i, j, sum;
    double kr;

    for (i = 0; i < D; i++) r[i] = 0;

    for (i = 0;;)
    {
        sum = 0;
        for (j = 0; j < D; j++) sum += k[j]*r[j];
        kr = a * sum;
        w->coskr[i] = cos(kr);
        w->sinkr[i] = sin(kr);
        if (++i == N) break;
        LATTICE_NEXT_SITE(r);
    }
}

double wavevector_m2(wavevector_t *w, const int spin[N])
{
    int i;
    double re = 0., im = 0.;

    for (i = 0; i < N; i++) re += spin[i] * w->coskr[i];
    for (i = 0; i < N; i++) im += spin[i] * w->sinkr[i];

    re /= N;
    im /= N;

    return re*re + im*im;
} 
