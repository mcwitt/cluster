/*
 * Wrapper for dSFMT random number generator. Contains all
 * implementation-specific code
 */

#ifndef RANDOM_H
#define RANDOM_H

#include <stdint.h>
#include "dSFMT.h"

/* define generic RNG type */
#define RANDOM_INIT(rng, seed) dsfmt_init_gen_rand(rng, seed)
#define RANDOM(rng) dsfmt_genrand_close_open(rng)

/* macro to read/write RNG state for checkpointing */
#define RANDOM_RW_STATE(rwfunc, fp, i, dsfmt) {\
    for (i = 0; i < DSFMT_N + 1; i++) {\
        rwfunc(fp, "%u ", dsfmt.status[i].u32[0]);\
        rwfunc(fp, "%u ", dsfmt.status[i].u32[1]);\
        rwfunc(fp, "%u ", dsfmt.status[i].u32[2]);\
        rwfunc(fp, "%u ", dsfmt.status[i].u32[3]);\
    }\
    rwfunc(fp, "%d ", dsfmt.idx);\
}

typedef dsfmt_t random_t;
typedef uint32_t random_seed_t;

#endif
