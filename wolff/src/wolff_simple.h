/* Wolff algorithm simulation of the Ising model */

#ifndef WOLFF_H
#define WOLFF_H

#include "ising.h"
#include "random.h"
#include <math.h>

/* probability to add a site to the cluster at a given temperature T */
#define WOLFF_P_ADD(T)  (1. - exp(-2./(T)))

typedef struct
{
    int spin[N];    /* spin[i] = +/- 1 */
    int stack[N];   /* used to build cluster */
    int c;          /* size of last cluster */
    int m;          /* magnetization */
    int ms;         /* magnetization of surface spins */
} wolff_t;

/* reset all spins to up */
void wolff_reset(wolff_t *w);

/* do a single Wolff cluster update */
void wolff_update(wolff_t *w, double p_add,
        int neighbor[N][Z], random_t *rng);

#endif
