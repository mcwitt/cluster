/*
 * Wolff algorithm simulation of the Ising model on a hypercubic lattice with
 * free boundary conditions.
 */

#include "ising.h"
#include "omp.h"
#include "random.h"
#include "wolff.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#define BCTYPE      "f"
#define NTEMP_MAX   100
#define NTHREAD_MAX 16

/* input parameters */
enum
{
    TFILE,
    NOCTAVE,
    SEEDS,
    NPARAM
};

/* measurements */
enum
{
    M,
    M2,
    M4,
    MABS,
    MP,
    MP2,
    MP4,
    MPABS,
    C,
    C2,
    C3,
    U, 
    U2,
    M2U,
    M4U,
    M2U2,
    M4U2,
    MP2U,
    MP4U,
    MP2U2,
    MP4U2,
    NMEAS
};

ising_t ising;

int main(int argc, char *argv[])
{
    FILE *fp;
    wolff_t *wolffp[NTHREAD_MAX];
    char *param_name[NPARAM], **param = &argv[1];
    double temps[NTEMP_MAX];
    int i, iparam, iseed,
        noct, nseed, ntemp,
        nthread = omp_get_max_threads(),
        noct_max = (int) (log(WOLFF_IMAX) / M_LN2);

    param_name[TFILE]   = "temperature file";
    param_name[NOCTAVE] = "number of octaves";
    param_name[SEEDS]   = "seeds...";

    if (nthread > NTHREAD_MAX)
    {
        fprintf(stderr, "%s: can't run with more than %d parallel threads\n",
                argv[0], NTHREAD_MAX);

        return EXIT_FAILURE;
    }

    if (argc < NPARAM+1)
    {
        fprintf(stderr, "Usage: %s ", argv[0]);

        for (iparam = 0; iparam < NPARAM; iparam++)
            fprintf(stderr, "<%s> ", param_name[iparam]);

        fprintf(stderr, "\nBuilt-in parameters:\n");
        fprintf(stderr, "  %-12s= \"%s\"\n", "bc", BCTYPE);
        fprintf(stderr, "  %-12s= %d\n", "d",  D);
        fprintf(stderr, "  %-12s= %d\n", "L",  L);
        fprintf(stderr, "  %-12s= %d\n", "noct_max", noct_max);

        return EXIT_FAILURE;
    }

    noct = atoi(param[NOCTAVE]);

    if ((noct < 1) || (noct > noct_max))
    {
        fprintf(stderr, "%s: must have 0 < number of octaves < %d\n",
                argv[0], noct_max);

        return EXIT_FAILURE;
    }

    if ((fp = fopen(param[TFILE], "r")) == NULL)
    {
        fprintf(stderr, "%s: error opening file %s\n", argv[0], param[TFILE]);
        return EXIT_FAILURE;
    }

    for (ntemp = 0; ntemp < NTEMP_MAX; ntemp++)
        if (fscanf(fp, "%lf", &temps[ntemp]) == EOF) break;

    if (ntemp == NTEMP_MAX)
    {
        fprintf(stderr,
                "%s: number of input temperatures must not exceed %d\n",
                argv[0], NTEMP_MAX);

        return EXIT_FAILURE;
    }

    nseed = argc - NPARAM;
    ising_init(&ising);

    printf("%4s %4s %4s %10s %10s %4s "
           "%14s %14s %14s "
           "%14s %14s %14s "
           "%14s %14s %14s "
           "%14s %14s "
           "%14s %14s %14s %14s "
           "%14s %14s %14s %14s\n",
           "bc", "d", "L", "seed", "T", "oct",
           "m2",  "m4",  "mabs", 
           "mp2", "mp4", "mpabs", 
           "c", "c2", "c3",
           "u", "u2",
           "m2u", "m4u", "m2u2", "m4u2",
           "mp2u", "mp4u", "mp2u2", "mp4u2");

    /* allocate memory */
    for (i = 0; i < nthread; i++)
        wolffp[i] = malloc(sizeof(wolff_t));

    #pragma omp parallel for
    for (iseed = 0; iseed < nseed; iseed++)
    {
        random_t rng;
        random_seed_t seed;
        wolff_t *wolff;
        double av[NMEAS], mv[NMEAS], p_add;
        int iclust, ioct, isweep, itemp, j,
            nclust_per_sweep, nflip, nsweep,
            ithread = omp_get_thread_num();

        wolff = wolffp[ithread];
        wolff_reset(wolff, &ising);
        seed = atoi(param[SEEDS+iseed]);
        RANDOM_INIT(&rng, seed);

        for (itemp = 0; itemp < ntemp; itemp++)
        {
            p_add = WOLFF_P_ADD(temps[itemp]);

            /* WARMUP -- approximate equilibrium cluster size */
            nflip = 0;
            nclust_per_sweep = 0;

            while (nflip < N)
            {
                wolff_update(wolff, p_add, ising.neighbor, &rng);
                nflip += wolff->c;
                nclust_per_sweep++;
            }

            nsweep = 1;

            for (ioct = 0; ioct <= noct; ioct++)
            {
                for (j = 0; j < NMEAS; j++) av[j] = 0.;

                /* nest loops to avoid integer overflow */
                for (isweep = 0; isweep < nsweep; isweep++)
                {
                    for (iclust = 0; iclust < nclust_per_sweep; iclust++)
                    {
                        wolff_update(wolff, p_add, ising.neighbor, &rng);

                        mv[M] = (double) wolff->m / N;
                        mv[M2] = mv[M] * mv[M];
                        mv[M4] = mv[M2] * mv[M2];
                        mv[MABS] = fabs(mv[M]);

                        mv[MP] = (double) (wolff->m - wolff->ms) / (N-NS);
                        mv[MP2] = mv[MP] * mv[MP];
                        mv[MP4] = mv[MP2] * mv[MP2];
                        mv[MPABS] = fabs(mv[MP]);

                        mv[C] = (double) wolff->c / N;
                        mv[C2] = mv[C] * mv[C];
                        mv[C3] = mv[C] * mv[C] * mv[C];

                        mv[U] = (double) wolff->e;
                        mv[U2] = mv[U] * mv[U];

                        mv[M2U]  = mv[M2] * mv[U];
                        mv[M4U]  = mv[M4] * mv[U];
                        mv[M2U2] = mv[M2] * mv[U2];
                        mv[M4U2] = mv[M4] * mv[U2];

                        mv[MP2U]  = mv[MP2] * mv[U];
                        mv[MP4U]  = mv[MP4] * mv[U];
                        mv[MP2U2] = mv[MP2] * mv[U2];
                        mv[MP4U2] = mv[MP4] * mv[U2];

                        for (j = 0; j < NMEAS; j++) av[j] += mv[j];
                    }
                }

                for (j = 0; j < NMEAS; j++)
                    av[j] = av[j] / nclust_per_sweep / nsweep;

                printf("%4s %4d %4d %10u %10.6f %4d "
                       "%14e %14e %14e "
                       "%14e %14e %14e "
                       "%14e %14e %14e "
                       "%14e %14e "
                       "%14e %14e %14e %14e "
                       "%14e %14e %14e %14e\n",
                        BCTYPE, D, L, seed, temps[itemp], ioct,
                        av[M2], av[M4], av[MABS],
                        av[MP2], av[MP4], av[MPABS],
                        av[C], av[C2], av[C3],
                        av[U], av[U2],
                        av[M2U], av[M4U], av[M2U2], av[M4U2],
                        av[MP2U], av[MP4U], av[MP2U2], av[MP4U2]);

                fflush(stdout);

                nclust_per_sweep = (int) (1. / av[C]);
                nsweep *= 2;

            }   /* end loop over octaves */
        }   /* end loop over temperatures */
    }   /* end loop over seeds */

    /* free memory */
    for (i = 0; i < nthread; i++) free(wolffp[i]);

    return EXIT_SUCCESS;
}
