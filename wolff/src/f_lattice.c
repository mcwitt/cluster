/* implementation of lattice.h for free lattices */

#include "lattice.h"
#include <math.h>

/* set table of neighbors given a list of vectors */
void lattice_set_neighbors(int *neighbor, int a[][D], int zmax)
{
    int Lpow[D], site[D], i_site, i_nghbr, j, k, nghbr_k, z;

    /* tabulate powers of L */
    Lpow[0] = 1; Lpow[1] = L;
    for (j = 2; j < D; j++) Lpow[j] = (int) pow(L, j);

    for (j = 0; j < D; j++) site[j] = 0;

    for (i_site = 0; i_site < N; i_site++)
    {
        z = 0;

        for (j = 0; j < zmax; j++)
        {
            i_nghbr = 0;

            for (k = 0; k < D; k++)
            {
                nghbr_k = site[k] + a[j][k];

                /* enforce free boundary conditions */
                if ((nghbr_k < 0) || (nghbr_k >= L)) break;
                i_nghbr += nghbr_k * Lpow[k];
            }

            if (k < D) continue;
            neighbor[i_site * zmax + z] = i_nghbr;
            z++;
        }

        if (z < zmax) neighbor[i_site * zmax + z] = -1;
        LATTICE_NEXT_SITE(site);
    }
}

