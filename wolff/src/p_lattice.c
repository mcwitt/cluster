/* implementation of lattice.h for periodic lattices */

#include "lattice.h"
#include <math.h>

/* set table of neighbors given a list of vectors */
void lattice_set_neighbors(int *neighbor, int a[][D], int z)
{
    int Lpow[D], r[D], isite, inghbr, j, k, s_k;

    /* tabulate powers of L */
    Lpow[0] = 1; Lpow[1] = L;
    for (j = 2; j < D; j++) Lpow[j] = (int) pow(L, j);

    for (j = 0; j < D; j++) r[j] = 0;

    for (isite = 0;;)
    {
        for (j = 0; j < z; j++)
        {
            inghbr = 0;

            for (k = 0; k < D; k++)
            {
                s_k = r[k] + a[j][k];

                /* enforce periodic boundary conditions */
                if (s_k < 0) s_k += L;
                else if (s_k >= L) s_k -= L;

                inghbr += s_k * Lpow[k];
            }

            neighbor[isite * z + j] = inghbr;
        }

        if (++isite == N) break;
        LATTICE_NEXT_SITE(r);
    }
}

