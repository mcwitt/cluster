#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define RANGE 9999999
#define WIDTH 7

typedef unsigned int UINT;

static UINT modular_pow(UINT base, UINT exponent, UINT modulus)
{
    UINT result = 1;

    while (exponent > 0)
    {
        if (exponent % 2 == 1) result = (result * base) % modulus;
        exponent = exponent >> 1;
        base = (base * base) % modulus;
    }

    return result;
}

int main()
{
    pid_t pid = getpid();
    printf("%0*u\n", WIDTH, modular_pow(pid, time(NULL), RANGE));
    return EXIT_SUCCESS;
}
