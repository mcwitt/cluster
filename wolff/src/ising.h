#ifndef ISING_H
#define ISING_H

#include "lattice.h"

typedef struct
{
    int neighbor[N][Z];
    int energy_ini;
} ising_t;

void ising_init(ising_t *s);
int ising_energy(ising_t *s, const int spin[N]);

#endif
