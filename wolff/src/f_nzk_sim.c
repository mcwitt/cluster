/*
 * Wolff algorithm simulation of the Ising model on a hypercubic lattice with
 * free boundary conditions.
 */

#include "ising.h"
#include "nzk.h"
#include "random.h"
#include "f_wavevector.h"
#include "wolff_simple.h"

#include "omp.h"
#include <errno.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BCTYPE      "f"
#define COL_WIDTH   14
#define NOCT_WARMUP 3
#define NTEMP_MAX   100
#define NTHREAD_MAX 64

/* input parameters */
enum
{
    TFILE,
    NOCTAVE,
    SEEDS,
    NPARAM
};

const char *param_name[] = {
    [TFILE]   = "temperature file",
    [NOCTAVE] = "number of octaves",
    [SEEDS]   = "seeds..."
};

enum    /* k-independent measurements */
{
    C,      /* moments of cluster size */
    C2,
    C3,
    U,      /* moments of energy */
    U2,
    MABS,   /* absolute value of magnetization */
    MABSU,
    MABSU2,
    MP2,    /* moments of magnetization excluding surface spins */
    MP4,
    MP2U,
    MP2U2,
    MP4U,
    MP4U2,
    MPABS,
    MPABSU,
    MPABSU2,

    NUM_MEAS_KIND
};

enum    /* k-dependent measurements */
{
    M2,     /* moments of magnetization */
    M4,
    M2U,    /* energy correlations */
    M2U2,
    M4U,
    M4U2,

    NUM_MEAS_KDEP
};

/* use constant names to label columns in output */
#define V(x) [x] = #x

const char *meas_name_kind[] = {
    V(C),
    V(C2),
    V(C3),
    V(U),
    V(U2),
    V(MABS),
    V(MABSU),
    V(MABSU2),
    V(MP2),
    V(MP4),
    V(MP2U),
    V(MP2U2),
    V(MP4U),
    V(MP4U2),
    V(MPABS),
    V(MPABSU),
    V(MPABSU2)
};

const char *meas_name_kdep[] = {
    V(M2),
    V(M4),
    V(M2U),
    V(M2U2),
    V(M4U),
    V(M4U2),
};

static void measure_kind(ising_t *ising, wolff_t *wolff, double meas[])
{
    /* k-independent measurements */

    double m;

    meas[C2] = meas[C] * meas[C];
    meas[C3] = meas[C] * meas[C2];

    meas[U] = (double) ising_energy(ising, wolff->spin);
    meas[U2] = meas[U] * meas[U];

    m = (double) wolff->m / N;

    meas[MABS]   = fabs(m);
    meas[MABSU]  = meas[MABS] * meas[U];
    meas[MABSU2] = meas[MABS] * meas[U2];

    /* measurements excluding surface spins */

    m = (double) (wolff->m - wolff->ms) / (N - NS);

    meas[MP2] = m * m;
    meas[MP4] = meas[MP2] * meas[MP2];

    meas[MP2U]  = meas[MP2] * meas[U];
    meas[MP2U2] = meas[MP2] * meas[U2];
    meas[MP4U]  = meas[MP4] * meas[U];
    meas[MP4U2] = meas[MP4] * meas[U2];

    meas[MPABS]   = fabs(m);
    meas[MPABSU]  = meas[MPABS] * meas[U];
    meas[MPABSU2] = meas[MPABS] * meas[U2];
}

static void measure_k0(wolff_t *wolff, double u, double u2, double meas[])
{
    /* measurments for k = 0 */

    double m = (double) wolff->m / N;

    meas[M2] = m * m;
    meas[M4] = meas[M2] * meas[M2];

    meas[M2U] = meas[M2] * u;
    meas[M4U] = meas[M4] * u;

    meas[M2U2] = meas[M2] * u2;
    meas[M4U2] = meas[M4] * u2;
}

static void measure_kdep(wolff_t *wolff, wavevector_t *w[D], int n,
        double u, double u2, double meas[])
{
    /* measurements for k^2 > 0 */

    double m2;
    int i;

    meas[M2] = 0.;
    meas[M4] = 0.;

    /* average over n equivalent wavevectors */
    for (i = 0; i < n; i++)
    {
        m2 = wavevector_m2(w[i], wolff->spin);
        meas[M2] += m2;
        meas[M4] += m2 * m2;
    }

    meas[M2] /= n;
    meas[M4] /= n;

    meas[M2U] = meas[M2] * u;
    meas[M4U] = meas[M4] * u;

    meas[M2U2] = meas[M2] * u2;
    meas[M4U2] = meas[M4] * u2;
}

int main(int argc, char *argv[])
{
    FILE *fp;
    ising_t *ising;
    wavevector_t *w[NZK_NUM][D];
    wolff_t *wolffp[NTHREAD_MAX];
    char **param = &argv[1];
    double temps[NTEMP_MAX];

    int ik, iseed, j, noct, nseed, ntemp,
        noct_max = (int) (log(LONG_MAX / N) / M_LN2),
        nsweep_warmup = (int) pow(2, NOCT_WARMUP),
        nthread = omp_get_max_threads();

    long nflip_warmup = N * nsweep_warmup;

    if (nthread > NTHREAD_MAX)
    {
        fprintf(stderr, "%s: can't run with more than %d parallel threads\n",
                argv[0], NTHREAD_MAX);

        return EXIT_FAILURE;
    }

    if (argc <= NPARAM)
    {
        fprintf(stderr, "Usage: %s ", argv[0]);
        for (j = 0; j < NPARAM; j++) fprintf(stderr, "<%s> ", param_name[j]);
        fprintf(stderr, "\n\n");
        fprintf(stderr, "%-12s= %d\n", "NTEMP_MAX", NTEMP_MAX);
        fprintf(stderr, "%-12s= %d\n", "NTHREAD_MAX", NTHREAD_MAX);
        fprintf(stderr, "\n");
        return EXIT_FAILURE;
    }

    noct = atoi(param[NOCTAVE]);

    if ((noct < NOCT_WARMUP) || (noct > noct_max))
    {
        fprintf(stderr, "%s: must have %d <= number of octaves <= %d\n",
                argv[0], NOCT_WARMUP, noct_max);

        return EXIT_FAILURE;
    }

    if ((fp = fopen(param[TFILE], "r")) == NULL)
    {
        fprintf(stderr, "%s: error opening file %s\n", argv[0], param[TFILE]);
        return EXIT_FAILURE;
    }

    for (ntemp = 0; ntemp < NTEMP_MAX; ntemp++)
        if (fscanf(fp, "%lf", &temps[ntemp]) == EOF) break;

    if (ntemp == NTEMP_MAX)
    {
        fprintf(stderr,
                "%s: number of input temperatures must not exceed %d\n",
                argv[0], NTEMP_MAX);

        return EXIT_FAILURE;
    }

    nseed = argc - NPARAM;

    /* allocate memory */

    #define MALLOC(type, ptr) { \
        ptr = malloc(sizeof(type)); \
        if (ptr == NULL) { \
            fprintf(stderr, "%s: error allocating memory for %s: %s\n", \
                    argv[0], #type, strerror(errno)); \
            return EXIT_FAILURE; \
        } \
    }
        
    MALLOC(ising_t, ising);

    for (ik = 0; ik < NZK_NUM; ik++)
        for (j = 0; j < nzk_num_ind[ik]; j++)
            MALLOC(wavevector_t, w[ik][j]);

    for (j = 0; j < nthread; j++) MALLOC(wolff_t, wolffp[j]);

    ising_init(ising);  /* init nearest neighbors */

    /* init sin and cos tables */
    for (ik = 0; ik < NZK_NUM; ik++)
        for (j = 0; j < nzk_num_ind[ik]; j++)
            wavevector_init(w[ik][j], nzk[ik][j]);

    /* print output header */
    printf("%16s %4s %4s %4s "
           "%10s %10s %4s %12s ",
           "ver", "bc", "d", "L",
           "seed", "T", "oct", "cps");

    for (j = 0; j < NUM_MEAS_KIND; j++) printf("%*s ", COL_WIDTH, meas_name_kind[j]);
    printf("%8s ", "k");
    printf("%4s ", "k^2");
    for (j = 0; j < NUM_MEAS_KDEP; j++) printf("%*s ", COL_WIDTH, meas_name_kdep[j]);
    printf("\n");

    /* BEGIN PARALLELIZED LOOP OVER SEEDS */
    #pragma omp parallel for private(ik, j)
    for (iseed = 0; iseed < nseed; iseed++)
    {
        random_t rng;
        random_seed_t seed;
        wolff_t *wolff;

        double kim[NUM_MEAS_KIND],  /* k-independent measurements */
               kia[NUM_MEAS_KIND],  /* k-independent averages */
               k0m[NUM_MEAS_KDEP],  /* measurements for k = 0 */
               k0a[NUM_MEAS_KDEP],  /* averages for k = 0 */
               kdm[NUM_MEAS_KDEP],  /* measurements for k^2 > 0 */
               kda[NZK_NUM][NUM_MEAS_KDEP], /* averages for k^2 > 0 */
               p_add;   /* probability of adding a spin to the cluster */

        long nflip, nflip_oct;

        int ioct, isweep, itemp, nclust_sweep, nsweep,
            ithread = omp_get_thread_num();

        wolff = wolffp[ithread];
        wolff_reset(wolff);
        seed = atoi(param[SEEDS + iseed]);
        RANDOM_INIT(&rng, seed);

        for (itemp = 0; itemp < ntemp; itemp++)
        {
            p_add = WOLFF_P_ADD(temps[itemp]);

            /* WARMUP -- approximate equilibrium cluster size */
            nflip = nclust_sweep = 0;

            while (nflip < nflip_warmup)
            {
                wolff_update(wolff, p_add, ising->neighbor, &rng);
                nflip += wolff->c;
                nclust_sweep++;
            }

            nclust_sweep /= nsweep_warmup;
            nsweep = nsweep_warmup;

            /* BEGIN MEASUREMENTS */
            for (ioct = NOCT_WARMUP+1; ioct <= noct; ioct++)
            {
                nflip_oct = 0;

                /* reset measurement sums */

                for (j = 0; j < NUM_MEAS_KIND; j++) kia[j] = 0.;
                for (j = 0; j < NUM_MEAS_KDEP; j++) k0a[j] = 0.;

                for (ik = 0; ik < NZK_NUM; ik++)
                   for (j = 0; j < NUM_MEAS_KDEP; j++)
                       kda[ik][j] = 0.;

                for (isweep = 0; isweep < nsweep; isweep++)
                {
                    nflip = 0;

                    for (j = 0; j < nclust_sweep; j++)
                    {
                        wolff_update(wolff, p_add, ising->neighbor, &rng);
                        nflip += wolff->c;
                    }

                    kim[C] = (double) nflip / nclust_sweep / N;
                    measure_kind(ising, wolff, kim);
                    measure_k0(wolff, kim[U], kim[U2], k0m);

                    for (j = 0; j < NUM_MEAS_KIND; j++) kia[j] += kim[j];
                    for (j = 0; j < NUM_MEAS_KDEP; j++) k0a[j] += k0m[j];

                    for (ik = 0; ik < NZK_NUM; ik++)
                    {
                        measure_kdep(wolff, w[ik], nzk_num_ind[ik], kim[U], kim[U2], kdm);
                        for (j = 0; j < NUM_MEAS_KDEP; j++) kda[ik][j] += kdm[j];
                    }

                    nflip_oct += nflip;
                }

                /* prevent threads from writing output simultaneously */
                #pragma omp critical
                {
                    /* write averages for k^2 = 0 */

                    printf("%16s %4s %4d %4d "
                           "%10u %10.6f %4d %12.6g ",
                            VERSION, BCTYPE, D, L,
                            seed, temps[itemp], ioct, (double) nclust_sweep);

                    for (j = 0; j < NUM_MEAS_KIND; j++)
                        printf("%*e ", COL_WIDTH, kia[j] / nsweep);

                    printf("%8s ", "0");    /* wavevector label */
                    printf("%4d ", 0);      /* wavevector norm */

                    for (j = 0; j < NUM_MEAS_KDEP; j++)
                        printf("%*e ", COL_WIDTH, k0a[j] / nsweep);

                    printf("\n");
                    fflush(stdout);

                    /* write averages for k^2 > 0 */

                    for (ik = 0; ik < NZK_NUM; ik++)
                    {
                        printf("%16s %4s %4d %4d "
                               "%10u %10.6f %4d %12.6g ",
                                VERSION, BCTYPE, D, L,
                                seed, temps[itemp], ioct, (double) nclust_sweep);

                        for (j = 0; j < NUM_MEAS_KIND; j++)
                            printf("%*e ", COL_WIDTH, kia[j] / nsweep);

                        printf("%8s ", nzk_label[ik]);
                        printf("%4d ", nzk_norm2[ik]);

                        for (j = 0; j < NUM_MEAS_KDEP; j++)
                            printf("%*e ", COL_WIDTH, kda[ik][j] / nsweep);

                        printf("\n");
                        fflush(stdout);
                    }
                }

                nclust_sweep = (int) (nsweep * (nclust_sweep / ((double) nflip_oct / N)));
                nsweep *= 2;

            }   /* end loop over octaves */
        }   /* end loop over temperatures */
    }   /* end loop over seeds */

    /* clean up and exit */
    free(ising);

    for (ik = 0; ik < NZK_NUM; ik++)
        for (j = 0; j < nzk_num_ind[ik]; j++)
            free(w[ik][j]);

    for (j = 0; j < nthread; j++) free(wolffp[j]);

    return EXIT_SUCCESS;
}
