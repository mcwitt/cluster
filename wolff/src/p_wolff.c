/* Wolff algorithm simulation of the Ising model on an arbitrary lattice */

#include "wolff.h"

void wolff_reset(wolff_t *w, ising_t *s)
{
    int j;

    /* reset spins and flip times */
    for (j = 0; j < N; j++) { w->spin[j] = 1; w->last[j] = 0; }

    w->e = s->energy_ini;
    w->m = N;
    w->nupd = 1;    /* reset timer */
}

void wolff_update(wolff_t *w, double p_add,
        int neighbor[N][Z], random_t *rng)
{
    int j, cluster_spin, site, nghbr, stack_size = 0;

    /* choose random seed spin and add to cluster */
    site = (int) (RANDOM(rng) * N);
    cluster_spin = w->spin[site];

    w->c = 1;                   /* set cluster size */
    w->last[site] = w->nupd;    /* update flip time */

    for (;;)
    {
        w->spin[site] *= -1;

        /* loop over neighbors of current site */
        for (j = 0; j < Z; j++)
        {
            nghbr = neighbor[site][j];

            if (w->spin[nghbr] == cluster_spin)
            {
                w->e += 2;

                if ((w->last[nghbr] != w->nupd) && (RANDOM(rng) < p_add))
                {
                    w->c++;
                    w->last[nghbr] = w->nupd;
                    w->stack[stack_size++] = nghbr;
                }
            }
            else w->e -= 2;
        }

        if (stack_size == 0) break;
        site = w->stack[--stack_size];
    }

    /* update magnetization and surface magnetization */
    cluster_spin *= 2;
    w->m  -= cluster_spin * w->c;

    w->nupd++;  /* increment time */
}

