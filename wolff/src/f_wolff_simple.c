/* Wolff algorithm simulation of the Ising model on an arbitrary lattice */

#include "wolff_simple.h"

void wolff_reset(wolff_t *w)
{
    int j;

    for (j = 0; j < N; j++) w->spin[j] = 1;
    w->m  = N;
    w->ms = NS;
}

void wolff_update(wolff_t *w, double p_add,
        int neighbor[N][Z], random_t *rng)
{
    int j, cluster_spin, site, nghbr, stack_size = 0, surface_flips = 0;

    /* choose random seed spin and add to cluster */
    site = (int) (RANDOM(rng) * N);
    cluster_spin = w->spin[site];

    w->c = 1;
    w->spin[site] *= -1;

    for (;;)
    {
        /* loop over neighbors of current site */
        for (j = 0; j < Z; j++)
        {
            nghbr = neighbor[site][j];

            if (nghbr == -1)  /* site is on the surface */
            {
                surface_flips++;
                break;
            }

            if ((w->spin[nghbr] == cluster_spin) && (RANDOM(rng) < p_add))
            {
                w->c++;
                w->spin[nghbr] *= -1;
                w->stack[stack_size++] = nghbr;
            }
        }

        if (stack_size == 0) break;
        site = w->stack[--stack_size];
    }

    /* update magnetization and surface magnetization */
    cluster_spin *= 2;
    w->m  -= cluster_spin * w->c;
    w->ms -= cluster_spin * surface_flips;
}

