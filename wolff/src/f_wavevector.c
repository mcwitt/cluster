#include "f_wavevector.h"
#include <math.h>

const double a = M_PI / (L + 1);

void wavevector_init(wavevector_t *w, const int k[D])
{
    int r[D], i, j;

    for (i = 0; i < D; i++) r[i] = 0;

    for (i = 0;;)
    {
        w->x[i] = 1.;
        for (j = 0; j < D; j++) w->x[i] *= sin(a*k[j]*(r[j]+1));
        if (++i == N) break;
        LATTICE_NEXT_SITE(r);
    }
}

double wavevector_m2(wavevector_t *w, const int spin[N])
{
    int i;
    double sum = 0.;

    for (i = 0; i < N; i++) sum += spin[i] * w->x[i];
    sum /= N;
    return sum * sum;
} 
