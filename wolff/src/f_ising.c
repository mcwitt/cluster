#include "ising.h"
#include <math.h>

void ising_init(ising_t *s)
{
    int a[Z][D], i, j;

    for (i = 0; i < D; i++)
    {
        for (j = 0; j < D; j++) a[i][j] = a[i+D][j] = 0;
        a[i][i] = -1;
        a[i+D][i] = 1;
    }

    lattice_set_neighbors(&s->neighbor[0][0], a, Z);
    s->energy_ini = -N*D + D*((int) pow(L, D-1));
}

int ising_energy(ising_t *s, const int spin[N])
{
    int i, j, nghbr, ex2 = 0; 
    
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < Z; j++)
        {
            nghbr = s->neighbor[i][j];
            if (nghbr == -1) break;
            ex2 -= spin[i]*spin[nghbr];
        }
    }

    return ex2 / 2;
}

