#!/bin/bash

template="job-mpipks.sh.in"
jobscript="job.$$.sh"

echo -n "D=? [5] "; read D; D=${D:-5}
echo -n "L=? [8] "; read L; L=${L:-8}
echo -n "Job ID? [0] "; read id; id=${id:-0}

echo -n "Number of 8-core jobs? [12] "
read num_jobs; num_jobs=${num_jobs:-12}

echo -n "Reserve memory? [8G] "
read h_rss; h_rss=${h_rss:-"8G"}

echo -n "Reserve CPU time? [10:00:00] "
read h_cpu; h_cpu=${h_cpu:-"24:00:00"}

# default values

sed -e "s/@D@/$D/"\
    -e "s/@L@/$L/"\
    -e "s/@id@/$id/"\
    -e "s/@num_jobs@/$num_jobs/"\
    -e "s/@h_rss@/$h_rss/"\
    -e "s/@h_cpu@/$h_cpu/"\
    $template > $jobscript

qsub $jobscript

