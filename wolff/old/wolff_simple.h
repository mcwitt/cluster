/*
 * Wolff algorithm simulation of the Ising model on an arbitrary lattice with
 * fixed coordination number Z.
 */

#include "defs.h"
#include "random.h"

typedef struct
{
    int spin[N];            /* spin[i] = +/- 1 */
    int neighbor[N][Z];     /* neighbor[i][0..Z-1] neighbors of site i */

    int cluster[N];         /* cluster flipped in last call to wolff_update
                               cluster[i] = 1 if site i in cluster, else 0 */
    int cluster_list[N];    /* list of sites in last cluster */
    int cluster_size;       /* size of last cluster */

    int m;                  /* magnetization */
    double p_add;           /* probability to add a site to the cluster */
} wolff_t;

/* call first to initialize */
void wolff_init(wolff_t *w, random_seed_t seed);

/* set temperature */
void wolff_set_T(wolff_t *w, double T);

/* do a Wolff update */
void wolff_update(wolff_t *w);

/* do a Wolff update without updating cluster variables in w */
void wolff_update_fast(wolff_t *w);
