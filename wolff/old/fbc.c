/*
 * Wolff algorithm simulation of the Ising model on a hypercubic lattice with
 * free boundary conditions.
 */

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "defs.h"
#include "wolff.h"

wolff_t w;
int Lpow[D];

/* set table of neighbors given a list of relative vectors */
void set_neighbors(int *neighbor, int r[][D], int zmax)
{
    int site[D], nghbr, xk, i, j, k, z;

    for (i = 0; i < D; i++) site[i] = 0;

    for (i = 0; i < N; i++)
    {
        z = 0;

        for (j = 0; j < zmax; j++)
        {
            nghbr = 0;

            for (k = 0; k < D; k++)
            {
                xk = site[k] + r[j][k];
                if ((xk < 0) || (xk >= L)) break;
                nghbr += xk * Lpow[k];
            }

            if (k < D) continue;
            neighbor[i*zmax + z] = nghbr;
            z++;
        }

        if (z < zmax) neighbor[i*zmax + z] = -1;

        j = 0;
        while (site[j] == L-1) site[j++] = 0;
        site[j]++;
    }
}

int main(int argc, char *argv[])
{
    int r[Z][D];
    int i, j, irun, iT, ibin, isamp, t, nT, nbin, nrun, binsize, tequil, seed;
    double T, Tmin, Tmax, dT, m2, m2av, m4av, mp2, mp2av, mp4av, mabs_av,
           n_av, n2av, n3av, cputime, N2 = (double) N*N, Np2 = pow(N - NS, 2);
    clock_t start = clock();

    if (argc != 9)
    {
        fprintf(stderr, "%s Tmin Tmax dT t_equil binsize nbins nruns seed\n"
                "d=%d, L=%d\n", argv[0], D, L);
        return EXIT_FAILURE;
    }

    /* read parameters from command line */
    Tmin    = atof(argv[1]);
    Tmax    = atof(argv[2]);
    dT      = atof(argv[3]);
    tequil  = atof(argv[4]);
    binsize = atoi(argv[5]);
    nbin    = atoi(argv[6]);
    nrun    = atoi(argv[7]);
    seed    = atoi(argv[8]);

    wolff_init(&w, seed);

    /* init global variables */
    Lpow[0] = 1; Lpow[1] = L;
    for (i = 2; i < D; i++) Lpow[i] = (int) pow(L, i);

    /* set nearest neighbors */
    for (i = 0; i < D; i++)
    {
        for (j = 0; j < D; j++) r[i][j] = r[i+D][j] = 0;
        r[i][i] = -1;
        r[i+D][i] = 1;
    }
    set_neighbors(&w.neighbor[0][0], r, Z);

    printf("# %2s %4s %8s %8s %8s %8s %8s "
            "%5s %8s %5s %13s %13s %13s %13s "
            "%13s %13s %13s %13s\n",
            "d", "L", "t_equil", "t_samp", "binsize", "seed", "cputime",
            "run", "T", "bin", "m^2", "m^4", "mp^2", "mp^4",
            "n", "n^2", "n^3", "|m|");

    /* loop over temperatures */
    if (fabs(dT) < 10e-9) nT = 1;
    else nT = (int) ((Tmax - Tmin)/dT) + 1;

    for (irun = 0; irun < nrun; irun++)
    {
        for (iT = 0; iT < nT; iT++)
        {
            T = Tmin + iT*dT;
            wolff_set_T(&w, T);

            /* equilibrate */
            for (t = 0; t < tequil; t++) wolff_update_fast(&w);

            /* loop over bins */
            for (ibin = 0; ibin < nbin; ibin++)
            {
                m2av = m4av = mp2av = mp4av = n_av = n2av = n3av = mabs_av = 0.;

                /* loop over measurements */
                for (isamp = 0; isamp < binsize; isamp++)
                {
                    wolff_update_fast(&w);

                    m2 = (double) w.m * w.m;
                    m2av += m2;
                    m4av += m2 * m2;

                    mp2 = (double) (w.m - w.ms);
                    mp2 *= mp2;
                    mp2av += mp2;
                    mp4av += mp2 * mp2;

                    n_av += w.cluster_size;
                    n2av += (double) w.cluster_size * w.cluster_size;
                    n3av += (double) w.cluster_size * w.cluster_size * w.cluster_size;

                    mabs_av += fabs((double) w.m);
                }

                m2av = m2av / N2 / binsize;
                m4av = m4av / N2 / N2 / binsize;
                mp2av = mp2av / Np2 / binsize;
                mp4av = mp4av / Np2 / Np2 / binsize;
                n_av = n_av / N / binsize;
                n2av = n2av / N2 / binsize;
                n3av = n3av / N2 / N / binsize;
                mabs_av = mabs_av / N / binsize;
                
                cputime = (clock() - start) / ((double) CLOCKS_PER_SEC);

                printf("%4d %4d %8d %8d %8d %8d %8g "
                       "%5d %8.4f %5d %13.5e %13.5e %13.5e %13.5e "
                       "%13.5e %13.5e %13.5e %13.5e\n",
                        D, L, tequil, 1, binsize, seed, cputime,
                        irun, T, ibin, m2av, m4av, mp2av, mp4av,
                        n_av, n2av, n3av, mabs_av);
                fflush(stdout);
            }
        }

        wolff_reset(&w);
    }

    return EXIT_SUCCESS;
}
