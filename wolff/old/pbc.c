/*
 * Wolff algorithm simulation of the Ising model on a hypercubic lattice with
 * periodic boundary conditions.
 */

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "defs.h"
#include "wolff_fz.h"

wolff_t w;
int partner[N]; /* measure correlation between spin i and partner[i] */
int Lpow[D];

/*
 * set table of neighbors given a list of relative vectors
 * must have -L < r[i][j] < L.
 */
static void set_neighbors(int *neighbor, int r[][D], int z)
{
    int site[D], nghbr, xk, i, j, k;

    for (i = 0; i < D; i++) site[i] = 0;

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < z; j++)
        {
            nghbr = 0;

            for (k = 0; k < D; k++)
            {
                xk = site[k] + r[j][k];

                /* enforce periodic boundary conditions */
                if (xk < 0) xk += L;
                else if (xk >= L) xk -= L;

                nghbr += xk * Lpow[k];
            }

            neighbor[i*z + j] = nghbr;
        }

        j = 0;
        while (site[j] == L-1) site[j++] = 0;
        site[j]++;
    }
}

/* measure G = < S_r S_{r+d} > averaged over sites r */
static double meas_G(int spin[N], int partner[N])
{
    int i, sum = 0;

    for (i = 0; i < N; i++) sum += spin[i] * spin[partner[i]];
    return (double) sum / N;
}

/* measure improved estimator for G (see Janke) */
static double meas_G_improved(int partner[N],
        int cluster[N], int cluster_list[N], int cluster_size)
{
    int i, sum = 0;

    for (i = 0; i < cluster_size; i++)
        sum += cluster[partner[cluster_list[i]]];

    return (double) sum / cluster_size;
}

int main(int argc, char *argv[])
{
    int r[Z][D];
    int i, j, iT, ibin, isamp, t, nT, nbin, binsize, tequil, tsamp, seed;
    double T, Tmin, Tmax, dT, m2, m2av, m4av, n_av, n2av, n3av,
           G, G_av, G2av, Gi_av, Gi2av, cputime, N2 = (double) N*N;
    clock_t start = clock();

    if (argc != 9)
    {
        fprintf(stderr, "%s Tmin Tmax dT t_equil t_samp binsize nbins seed\n"
                "t_equil : number of clusters for equilibration\n"
                "t_samp  : number of clusters between measurements\n"
                "d=%d, L=%d\n", argv[0], D, L);
        return EXIT_FAILURE;
    }

    /* read parameters from command line */
    Tmin    = atof(argv[1]);
    Tmax    = atof(argv[2]);
    dT      = atof(argv[3]);
    tequil  = atoi(argv[4]);
    tsamp   = atoi(argv[5]);
    binsize = atoi(argv[6]);
    nbin    = atoi(argv[7]);
    seed    = atoi(argv[8]);

    wolff_init(&w, seed);

    /* init global variables */
    Lpow[0] = 1; Lpow[1] = L;
    for (i = 2; i < D; i++) Lpow[i] = (int) pow(L, i);

    /* set nearest neighbors */
    for (i = 0; i < D; i++)
    {
        for (j = 0; j < D; j++) r[i][j] = r[i+D][j] = 0;
        r[i][i] = -1;
        r[i+D][i] = 1;
    }
    set_neighbors(&w.neighbor[0][0], r, Z);

    /* set pairs for correlation function */
    for (i = 0; i < D; i++) r[0][i] = L/2;
    set_neighbors(partner, r, 1);

    printf("# %2s %4s %8s %8s %8s %8s %8s %8s %5s "
            "%13s %13s %13s %13s %13s "
            "%13s %13s %13s %13s\n",
            "d", "L", "t_equil", "t_samp", "binsize", "seed", "cputime", "T", "bin",
            "m^2", "m^4", "n", "n^2", "n^3",
            "G", "G^2", "Gi", "Gi^2");

    /* loop over temperatures */
    if (fabs(dT) < 10e-9) nT = 1;
    else nT = (int) ((Tmax - Tmin)/dT) + 1;

    for (iT = 0; iT < nT; iT++)
    {
        T = Tmin + iT*dT;
        wolff_set_T(&w, T);

        /* equilibrate */
        for (t = 0; t < tequil; t++) wolff_update_fast(&w);

        /* loop over bins */
        for (ibin = 0; ibin < nbin; ibin++)
        {
            m2av = m4av = 0.;
            n_av = n2av = n3av = 0.;
            G_av = G2av = 0.;
            Gi_av = Gi2av = 0.;

            /* loop over measurements */
            for (isamp = 0; isamp < binsize; isamp++)
            {
                for (t = 1; t < tsamp; t++) wolff_update_fast(&w);
                wolff_update(&w);

                m2 = (double) w.m * w.m;
                m2av += m2;
                m4av += m2 * m2;
                n_av += w.cluster_size;
                n2av += (double) w.cluster_size * w.cluster_size;
                n3av += (double) w.cluster_size * w.cluster_size * w.cluster_size;
                G = meas_G(w.spin, partner);
                G_av += G;
                G2av += G * G;
                G = meas_G_improved(partner, w.cluster,
                        w.cluster_list, w.cluster_size);
                Gi_av += G;
                Gi2av += G * G;
            }

            m2av = m2av / N2 / binsize;
            m4av = m4av / N2 / N2 / binsize;
            n_av = n_av / N / binsize;
            n2av = n2av / N2 / binsize;
            n3av = n3av / N2 / N / binsize;
            G_av /= binsize;
            G2av /= binsize;
            Gi_av /= binsize;
            Gi2av /= binsize;
            
            cputime = (clock() - start) / ((double) CLOCKS_PER_SEC);

            printf("%4d %4d %8d %8d %8d %8d %8g %8.4f %5d "
                    "%13.5e %13.5e %13.5e %13.5e %13.5e "
                    "%13.5e %13.5e %13.5e %13.5e\n",
                    D, L, tequil, tsamp, binsize, seed, cputime, T, ibin,
                    m2av, m4av, n_av, n2av, n3av,
                    G_av, G2av, Gi_av, Gi2av);
            fflush(stdout);
        }
    }

    return EXIT_SUCCESS;
}
