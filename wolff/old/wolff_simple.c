/*
 * Wolff algorithm simulation of the Ising model on an arbitrary lattice with
 * fixed coordination number Z.
 */

#include <math.h>
#include "wolff_simple.h"
#include "random.h"

static int stack[N];
static random_t rng;

void wolff_init(wolff_t *w, random_seed_t seed)
{
    int i;

    RANDOM_INIT(&rng, seed);  /* init random number generator */
    w->p_add = 0.;

    /* start with all spins up */
    for (i = 0; i < N; i++) w->spin[i] = 1;
    w->m = N;
}

void wolff_set_T(wolff_t *w, double T)
{
    w->p_add = 1. - exp(-2./T);
}

void wolff_update(wolff_t *w)
{
    int i, site, nghbr, cluster_spin, stack_size = 0;

    for (i = 0; i < N; i++) w->cluster[i] = 0; /* reset cluster */

    /* choose random seed spin and add to cluster */
    site = (int) (RANDOM(&rng) * N);
    cluster_spin = w->spin[site];
    w->spin[site] *= -1;
    w->cluster_size = 1;
    w->cluster[site] = 1;
    w->cluster_list[0] = site;

    for (;;)
    {
        for (i = 0; i < Z; i++) /* loop over neighbors of current site */
        {
            nghbr = w->neighbor[site][i];

            if ((w->spin[nghbr] == cluster_spin) && (RANDOM(&rng) < w->p_add))
            {
                w->spin[nghbr] *= -1;
                w->cluster[nghbr] = 1;
                w->cluster_list[w->cluster_size++] = nghbr;
                stack[stack_size++] = nghbr;
            }
        }

        if (stack_size == 0) break;
        site = stack[--stack_size];
    }

    w->m -= 2 * cluster_spin * w->cluster_size;
}

void wolff_update_fast(wolff_t *w)
{
    int i, site, nghbr, cluster_spin, stack_size = 0;

    /* choose random seed spin and add to cluster */
    site = (int) (RANDOM(&rng) * N);
    cluster_spin = w->spin[site];
    w->spin[site] *= -1;
    w->cluster_size = 1;

    for (;;)
    {
        for (i = 0; i < Z; i++) /* loop over neighbors of current site */
        {
            nghbr = w->neighbor[site][i];

            if ((w->spin[nghbr] == cluster_spin) && (RANDOM(&rng) < w->p_add))
            {
                w->spin[nghbr] *= -1;
                w->cluster_size++;
                stack[stack_size++] = nghbr;
            }
        }

        if (stack_size == 0) break;
        site = stack[--stack_size];
    }

    w->m -= 2 * cluster_spin * w->cluster_size;
}

