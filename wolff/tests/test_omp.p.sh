#! /bin/bash

name=test_omp.p

OMP_NUM_THREADS=2 ./p_sim test.p.temps 3 999 123 | egrep '\<123\>|#' > $name.out
diff -q $name.out test.p.ref && rm $name.out
