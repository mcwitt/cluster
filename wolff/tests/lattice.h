/* definitions and useful functions for dealing with hypercubic lattices */

#ifndef LATTICE_H
#define LATTICE_H

#include <limits.h>
#include <stdlib.h>

#define D  6
#define L  4
#define Z  12  /* Z = 2*D */
#define N  4096  /* N = L^D */
#define NS 4032 /* number of surface sites */

#if (N*D > INT_MAX)
#error System size too big!
#endif

/* move coordinates to next site */
#define LATTICE_NEXT_SITE(site) { \
    int i = 0; \
    while (site[i] == L-1) site[i++] = 0; \
    site[i]++; \
}

/* tabulate neighbors given a list of vectors */
void lattice_set_neighbors(int *neighbor, int a[][D], int zmax);

#endif
