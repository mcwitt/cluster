#! /bin/bash

name=test_omp.f

OMP_NUM_THREADS=2 ./f_sim test.f.temps 3 999 123 | egrep '\<123\>|#' > $name.out
diff -q $name.out test.f.ref && rm $name.out
